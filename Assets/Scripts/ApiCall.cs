﻿using Sfs2X.Entities.Variables;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ApiCall : MonoBehaviour
{
    static ApiCall instance_;
    void OnEnable()
    {
        instance_ = this;
    }

    static string joinCode = "";

    public static void OnJoin(string code)
    {
        joinCode = code;
        instance_.StartCoroutine(instance_.OnJoinApi());
    }

    IEnumerator OnJoinApi()
    {
        WWWForm form = new WWWForm();
        using (UnityWebRequest www = UnityWebRequest.Post("http://gametesting.in/sticky13dmo/api/join/game?join_code="+ joinCode + "&ip=192.168.0.1", form))
        {
            print(www.url);
            yield return www.SendWebRequest();
            Debug.Log(www.downloadHandler.text);
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {

                LoadData data = JsonUtility.FromJson<LoadData>(www.downloadHandler.text);
                if (data.status)
                {
                    UserData.token = data.tocken;
                    List<UserVariable> userVariable = new List<UserVariable>();
                    userVariable.Clear();
                    UserVariable token = new SFSUserVariable("token", UserData.token);
                    userVariable.Add(token);
                    SFS.UpdateUserVariable(userVariable);
                    GameLogin.Join(joinCode);
                }
                else
                {
                    GameLogin.ShowError(data.message);
                }
            }
        }
    }

    public static void CreateRoom(string roomId , string emailId)
    {
        instance_.StartCoroutine(instance_.CreateRoomApi(roomId , emailId));
    }

    IEnumerator CreateRoomApi(string roomId , string email)
    {
        WWWForm form = new WWWForm();
        using (UnityWebRequest www = UnityWebRequest.Post("http://gametesting.in/sticky13dmo/api/game/create?room_id=" + roomId + "&email="+ email, form))
        {
            print(www.url);
            yield return www.SendWebRequest();
            Debug.Log(www.downloadHandler.text);
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                LoadData data = JsonUtility.FromJson<LoadData>(www.downloadHandler.text);
                if (data.status)
                {

                    List<UserVariable> userVariable = new List<UserVariable>();
                    userVariable.Clear();
                    UserVariable token = new SFSUserVariable("token", "");
                    userVariable.Add(token);
                    SFS.UpdateUserVariable(userVariable);
                    UserData.token = data.tocken;
                    GameLogin.Create();
                }
                else
                {
                    GameLogin.ShowError(data.message);
                }

            }
        }
    }


    public class LoadData
    {
        public bool status;
        public string message;
        public string tocken;
    }
}
