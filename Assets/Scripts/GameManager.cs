﻿using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    static GameManager instance_;
    public static event System.Action onInfoLoaded;
    public static event System.Action OnCardLoaded;
    public static event System.Action OnOpenCardLoaded;
    public static event System.Action OnStartGameLoaded;
    public static event System.Action<bool> OnRestartGame;
    public static event System.Action gameComplete;

    public Text gameidText;

    public Button startGameButton;
    public Button cancelGameButton;

    public Button restartGameButton;

    public Button nextCardButton;

    public Button endGameButton;

    public Button lastcardButton;

    public Button sticky13Button;

    public GameObject vaidateText;

    void OnEnable()
    {
        instance_ = this;
        SFS.ExtensionResponseReceived += OnExtensionResponseRecieved;
        Common.OnJoinRoom += OnJoinRoom;
        if (startGameButton != null)
        {
            startGameButton.onClick.AddListener(StartGameClick);
        }

        if (cancelGameButton != null)
        {
            cancelGameButton.onClick.AddListener(cancelGameClick);
        }

        if (nextCardButton != null)
        {
            nextCardButton.onClick.AddListener(OnNextCardClick);
        }

        if (restartGameButton != null)
        {
            restartGameButton.onClick.AddListener(restartGame);
        }

        if (endGameButton != null)
        {
            endGameButton.onClick.AddListener(endGame);
        }

        if (sticky13Button != null)
        {
            sticky13Button.onClick.AddListener(OnSticky13Card);
        }

        if (lastcardButton != null)
        {
            lastcardButton.onClick.AddListener(OnLastCard);
        }




    }

    void OnDisable()
    {
        SFS.ExtensionResponseReceived -= OnExtensionResponseRecieved;
        Common.OnJoinRoom -= OnJoinRoom;
        if (startGameButton != null)
        {
            startGameButton.onClick.RemoveListener(StartGameClick);
        }

        if (cancelGameButton != null)
        {
            cancelGameButton.onClick.RemoveListener(cancelGameClick);
        }

        if (nextCardButton != null)
        {
            nextCardButton.onClick.RemoveListener(OnNextCardClick);
        }

        if (restartGameButton != null)
        {
            restartGameButton.onClick.RemoveListener(restartGame);
        }

        if (endGameButton != null)
        {
            endGameButton.onClick.RemoveListener(endGame);
        }

        if (sticky13Button != null)
        {
            sticky13Button.onClick.RemoveListener(OnSticky13Card);
        }


        if (lastcardButton != null)
        {
            lastcardButton.onClick.RemoveListener(OnLastCard);
        }

    }


    void OnSticky13Card()
    {
        sticky13Button.interactable = false;
        SFS.SendExtensionRequest("Sticky13", new SFSObject());
    }


    void OnLastCard()
    {
        lastcardButton.interactable = false;
        SFS.SendExtensionRequest("LastCard", new SFSObject());
    }

    void OnNextCardClick()
    {
        SFS.SendExtensionRequest("NextCard", new SFSObject());
        StopCoroutine("StartTimer");
        StartCoroutine("StartTimer");
    }
    public Image turnTimer;
    IEnumerator StartTimer()
    {
        nextCardButton.interactable = false;
        float tempVal = 0;
        while (tempVal < 5)
        {
            tempVal += Time.deltaTime;
            turnTimer.fillAmount = tempVal / 5f;
            yield return null;
        }

        if (LoadInfo.openCount < 52)
        {
            nextCardButton.interactable = true;
        }
    }

    void StartGameClick()
    {
        SFS.SendExtensionRequest("StartGame", new SFSObject());
    }

    void cancelGameClick()
    {
        SFS.SendExtensionRequest("CancelGame", new SFSObject());
    }

    void restartGame()
    {
        SFS.SendExtensionRequest("Restart", new SFSObject());
    }

    void endGame()
    {
        SFS.SendExtensionRequest("EndGame", new SFSObject());
    }

    void OnJoinRoom()
    {
        gameidText.text = GameStats.roomName;
        SFS.JoinRoom(GameStats.roomName , (Room room)=>
        {

        } , (string error)=>
        {

        });
    }

    void OnExtensionResponseRecieved(string cmd, SFSObject dataObject, Room room)
    {
        switch (cmd)
        {
            case "info":
                {
                    HandleInfo(dataObject);
                    break;
                }
            case "DistributeCard":
                {
                    HandleDistributeCard(dataObject);
                    break;
                }
            case "NextCard":
                {
                    HandleCard(dataObject);
                    break;
                }
            case "StartGame":
                {
                    OnStartGame(dataObject);
                    break;
                }
            case "CancelGame":
                {
                    HandleCancel(dataObject);
                    break;
                }
            case "Restart":
                {
                    HandleRestart();
                    break;
                }
            case "EndGame":
                {
                    HandleEndGame();
                    break;
                }
            case "Sticky13":
                {
                    OnSticky13(dataObject);
                    break;
                }
        }
    }

    void OnSticky13(SFSObject dataObject)
    {
        LoadWinner.winnerName = dataObject.GetUtfString("winner");
        LoadWinner.ownerName = dataObject.GetUtfString("ownerName");
        if (gameComplete != null)
        {
            gameComplete();
        }
        StopCoroutine("StartTimer");
        turnTimer.fillAmount = 0;
        vaidateText.gameObject.SetActive(true);
        nextCardButton.interactable = false;
    }

    public static class LoadWinner
    {
        public static string winnerName;
        public static string ownerName;
    }


    void HandleEndGame()
    {
        if (OnRestartGame != null)
        {
            OnRestartGame(false);
        }
    }


    void HandleRestart()
    {
        if (OnRestartGame != null)
        {
            OnRestartGame(true);
        }
    }

    void HandleCancel(SFSObject dataObject)
    {
       
        OnLeaveRoom();
    }

    

    public void OnLeaveRoom()
    {
        SFS.LeaveRoom(SFS.lastJoinedRoom, () =>
        {
            if (OnRestartGame != null)
            {
                OnRestartGame(true);
            }
            Common.OnUpdateCanvas(CanvasType.Login);
        }, (string error) =>
        {

        });
    }


    void OnStartGame(SFSObject dataObject)
    {
        if (dataObject.GetBool("status"))
        {
            nextCardButton.interactable = true;
            if (OnStartGameLoaded != null)
            {
                OnStartGameLoaded();
            }
        }
    }

    void HandleInfo(SFSObject dataObject)
    {
        LoadInfo.username = dataObject.GetUtfStringArray("username");
        LoadInfo.matchCount = dataObject.GetIntArray("matchCount");
        LoadInfo.isSticky = dataObject.GetBoolArray("isSticky");
        LoadInfo.status = dataObject.GetUtfStringArray("status");
        LoadInfo.isLastCard = dataObject.GetBoolArray("isLastCard");
        LoadInfo.ownerName = dataObject.GetUtfString("owner");
        LoadInfo.isGameStarted = dataObject.GetBool("isGameStarted");
        LoadInfo.openCount = dataObject.GetInt("openCount");
        LoadInfo.remaining = dataObject.GetInt("remaining");
        LoadInfo.remaining = dataObject.GetInt("remaining");


        if (onInfoLoaded != null)
        {
            onInfoLoaded();
        }
    }

    public static class LoadInfo
    {
        public static string[] username;
        public static int[] matchCount;
        public static bool[] isSticky;
        public static bool[] isLastCard;
        public static string[] status;
        public static string ownerName;
        public static bool isGameStarted;
        public static int openCount;
        public static int remaining;
    }

    void HandleDistributeCard(SFSObject dataObject)
    {
        LoadDistributeCard.username = dataObject.GetUtfStringArray("user");
        LoadDistributeCard.card = dataObject.GetUtfStringArray("cards");
        vaidateText.gameObject.SetActive(false);
        nextCardButton.interactable = true;
        if (OnCardLoaded != null)
        {
            OnCardLoaded();
        }
    }

    public static class LoadDistributeCard
    {
        public static string[] username;
        public static string[] card;
    }

    void HandleCard(SFSObject dataObject)
    {
        LoadOpenCard.openCard = dataObject.GetUtfString("openCard");
        if (OnOpenCardLoaded != null )
        {
            OnOpenCardLoaded();
        }
    }

    public static void OnDisableNextCard()
    {
        instance_.nextCardButton.interactable = false;
    }

    public static class LoadOpenCard
    {
        public static string openCard;
    }
}
