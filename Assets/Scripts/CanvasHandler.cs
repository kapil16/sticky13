﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasHandler : MonoBehaviour
{
    public CanvasType type;
    Canvas canvas;
    void OnEnable()
    {
        Common.canvasUpdated += OncanvasUpdate;
        canvas = GetComponent<Canvas>();
    }

    void OnDisable()
    {
        Common.canvasUpdated -= OncanvasUpdate;
    }

    void OncanvasUpdate(CanvasType type_)
    {
        print(" ---- ");
        if (type_ == type)
        {
            canvas.sortingOrder = 10;
        }
        else
        {
            canvas.sortingOrder = 5;
        }
    }



}
