﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NextCard : MonoBehaviour
{
    public Image cardSprite;
    string cardName_;
    public string cardName
    {
        get
        {
            return cardName_;
        }
        set
        {
            cardName_ = value;
            cardSprite.sprite = CardManager.GetCard(cardName_);
        }
    }

    public void reset()
    {
        cardSprite.sprite = CardManager.getBackCard();
    }

}
