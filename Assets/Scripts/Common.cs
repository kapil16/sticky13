﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Common : MonoBehaviour
{
    public static event System.Action<CanvasType> canvasUpdated;
    public static event System.Action OnJoinRoom;

    public static void OnUpdateCanvas(CanvasType type)
    {
        if (canvasUpdated != null)
        {
            canvasUpdated(type);
        }

        if (CanvasType.Game == type)
        {
            OnJoinRoom();
        }
    }

}
