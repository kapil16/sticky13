﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardManager : MonoBehaviour
{
    [System.Serializable]
    public class LoadCard
    {
        public string name;
        public Sprite sprite;
    }

    public LoadCard[] cards;

    public Sprite backcard;

    static CardManager instance_;

    void Awake()
    {
        instance_ = this;
    }

    void OnEnable()
    {

    }

    public static Sprite GetCard(string name)
    {
        Sprite sprite = null;
        for (int i=0;i<instance_.cards.Length;i++)
        {
            if (instance_.cards[i].name == name)
            {
                sprite = instance_.cards[i].sprite;
                break;
            }
        }

        return sprite;
    }

    public static Sprite getBackCard()
    {
        return instance_.backcard;
    }


}
