﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePlayerItemList : MonoBehaviour
{
    public Text usernameText;
    public Text missCountText;
    public Text statusText;

    string username_;
    public string username
    {
        get {
            return username_;
        }

        set
        {
            username_ = value;
            if (usernameText != null)
            {
                usernameText.text = username_;
            }
        }
    }
    
    int matchCount_;
    public int matchCount
    {
        get
        {
            return matchCount_;
        }
        set
        {
            matchCount_ = value;
            if (missCountText!= null)
            {
                missCountText.text = ""+ matchCount_;
            }
        }
    }

    string status_;
    public string status
    {
        get
        {
            return status_;
        }
        set
        {
            status_ = value;
            if (statusText != null)
            {
                statusText.text = "" + status_;
            }
        }
    }

}
