﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Reset : MonoBehaviour
{
    public Text[] text;
    public float value;

    void OnEnable()
    {
        StartCoroutine("reset");
    }
        
    IEnumerator reset()
    {
        yield return new WaitForSeconds(value);
        for (int i=0;i< text.Length;i++)
        {
            text[i].text = "";
            text[i].gameObject.SetActive(false);
        }
    }

}
