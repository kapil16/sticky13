﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardItemList : MonoBehaviour
{
    public Image cardSprite;
    string cardName_;
    public string cardName
    {
        get
        {
            return cardName_;
        }
        set
        {
            cardName_ = value;
            cardSprite.sprite = CardManager.GetCard(cardName);

            //cardSprite.sprite = CardManager.getBackCard();
        }
    }

    private Button cardButton;

    void Awake()
    {
        cardButton = cardSprite.GetComponent<Button>();
    }

    void OnEnable()
    {
        canFlip = false;
        GameManager.OnOpenCardLoaded += OnCardOpen;
        GameManager.gameComplete += OpenAllcardCard;
        if (cardButton != null)
        {
            cardButton.onClick.AddListener(OnClick);
        }
    }

    void OnClick()
    {
        if (!canFlip)
        {
            return;
        }
        canFlip = false;

        SFS.SendExtensionRequest("CardFlip", new Sfs2X.Entities.Data.SFSObject());
        cardSprite.sprite = CardManager.getBackCard();
    }

    void OnDisable()
    {
        GameManager.OnOpenCardLoaded -= OnCardOpen;
        GameManager.gameComplete -= OpenAllcardCard;
        if (cardButton != null)
        {
            cardButton.onClick.RemoveListener(OnClick);
        }
    }

    bool canFlip;
    void OnCardOpen()
    {
        //print("closing owner card");
        if (UserData.username == GameManager.LoadInfo.ownerName && cardName == GameManager.LoadOpenCard.openCard)
        {
            cardSprite.sprite = CardManager.getBackCard();
        }
        else
        {
            if (cardName == GameManager.LoadOpenCard.openCard)
            {
                canFlip = true;
            }
        }
        //if (cardName == GameManager.LoadOpenCard.openCard)
        //{
        //    cardSprite.sprite = CardManager.GetCard(cardName);
        //}
    }


    void OpenAllcardCard()
    {
        cardSprite.sprite = CardManager.GetCard(cardName);
    }


}
