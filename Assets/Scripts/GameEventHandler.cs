﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameEventHandler : MonoBehaviour
{
    public GameObject gamePlayerPrefab;
    public GameObject gamePlayerParent;

    public Text cardDrawnText;
    public Text cardRemainingText;

    public NextCard nextCard;

    public GameObject hostPlayerPrefab;
    public GameObject playerPrefab;
    public GameObject hostGamePanel;

    public GameObject stickyButton;
    public GameObject lastButton;

    public GameObject listOfplayerObj;

    public GameObject playernameObject;
    public GameObject turnObject;
    public Text playernameText;

    public GameObject youWinPopUp;
    
    public GameObject youLoosePopUp;

    public GameObject gameIsRuuning;


    [System.Serializable]
    public class LoadedCards
    {
        public int index;
        public GameObject[] cardObj;
    }

    public LoadedCards[] loadedCards;

    void OnEnable()
    {
        GameManager.onInfoLoaded += LoadInfo;
        GameManager.OnOpenCardLoaded += OpenCard;
        GameManager.OnCardLoaded += OnDistributeCard;
        GameManager.OnStartGameLoaded += OnStartGameLoaded;
        GameManager.OnRestartGame += OnRestartGame;
        GameManager.gameComplete += OnShowWinner;
    }

    void OnDisable()
    {
        GameManager.onInfoLoaded -= LoadInfo;
        GameManager.OnOpenCardLoaded -= OpenCard;
        GameManager.OnCardLoaded -= OnDistributeCard;
        GameManager.OnStartGameLoaded -= OnStartGameLoaded;
        GameManager.OnRestartGame -= OnRestartGame;
        GameManager.gameComplete -= OnShowWinner;
    }

    void OnShowWinner()
    {
        if (UserData.username == GameManager.LoadWinner.winnerName)
        {
            youWinPopUp.SetActive(true);
        }
    }

    void OnRestartGame(bool isRestart)
    {
        if(youWinPopUp != null)
            youWinPopUp.SetActive(false);

        if (youLoosePopUp != null)

            youLoosePopUp.SetActive(false);
        for (int i = 0; i < loadedCards.Length; i++)
        {
            for (int j = 0; j < loadedCards[i].cardObj.Length; j++)
            {
                loadedCards[i].cardObj[j].gameObject.SetActive(false);
            }
        }

        nextCard.reset();

        cardDrawnText.text = "Cards Drawn: 0";
        cardRemainingText.text = "Cards Left: 0";

        stickyButton.GetComponent<Button>().interactable = false;
        lastButton.GetComponent<Button>().interactable = false;
        if (!isRestart)
        {
            hostPlayerPrefab.SetActive(GameManager.LoadInfo.ownerName == UserData.username);
            hostGamePanel.SetActive(false);
        }
        else
        {
            hostPlayerPrefab.SetActive(false);
            hostGamePanel.SetActive(GameManager.LoadInfo.ownerName == UserData.username);
        }

        playerPrefab.SetActive(GameManager.LoadInfo.ownerName != UserData.username);
    }

    void OnStartGameLoaded()
    {
        hostPlayerPrefab.SetActive(false);
        hostGamePanel.SetActive(GameManager.LoadInfo.ownerName == UserData.username);
        playerPrefab.SetActive(GameManager.LoadInfo.ownerName != UserData.username);
        print("activating panel -- > ");
    }

    void LoadInfo()
    {
        bool isSticky_ = false;
        bool isLast_ = false;


        for (int i=gamePlayerParent.transform.childCount -1;i>0;i--)
        {
            Destroy(gamePlayerParent.transform.GetChild(i).gameObject);
        }



        for (int i = GameManager.LoadInfo.username.Length -1; i >=0 ; i--)
        {
            if (GameManager.LoadInfo.username[i] == GameManager.LoadInfo.ownerName)
            {
                continue;
            }

            if (GameManager.LoadInfo.username[i] == UserData.username && GameManager.LoadInfo.isSticky[i])
            {
                isSticky_ = true;
            }

            if (GameManager.LoadInfo.username[i] == UserData.username && GameManager.LoadInfo.isLastCard[i])
            {
                isLast_ = true;
            }


            GameObject obj = Instantiate(gamePlayerPrefab);
            obj.transform.SetParent(gamePlayerParent.transform, false);
            GamePlayerItemList gamePlayer = obj.GetComponent<GamePlayerItemList>();
            gamePlayer.username = GameManager.LoadInfo.username[i];
            gamePlayer.matchCount = GameManager.LoadInfo.matchCount[i];
            gamePlayer.status = GameManager.LoadInfo.status[i];
            obj.SetActive(true);

            //if (isPlayerPresent(GameManager.LoadInfo.username[i]))
            //{
            //    GamePlayerItemList gamePlayer = getgamePlayer(GameManager.LoadInfo.username[i]);
            //    gamePlayer.matchCount = GameManager.LoadInfo.matchCount[i];
            //    gamePlayer.status = GameManager.LoadInfo.status[i];
            //}
            //else
            //{
                
            //}
        }

        if (isSticky_)
        {
            stickyButton.GetComponent<Button>().interactable = true;
        }

        if (isLast_)
        {
            lastButton.GetComponent<Button>().interactable = true;
        }

        playernameObject.SetActive(UserData.username != GameManager.LoadInfo.ownerName);
        turnObject.SetActive(UserData.username == GameManager.LoadInfo.ownerName);

        print(UserData.username);
        print(GameManager.LoadInfo.ownerName);
        if (UserData.username == GameManager.LoadInfo.ownerName)
        {
            listOfplayerObj.SetActive(true);
            gameIsRuuning.SetActive(false);
        }
        else
        {
            if (!GameManager.LoadInfo.isGameStarted)
            {
                gameIsRuuning.SetActive(false);
            }
            playernameText.text = "Player Name: " + UserData.username;
            listOfplayerObj.SetActive(false);
        }

        for (int i = gamePlayerParent.transform.childCount - 1; i > 0; i--)
        {
            bool isPresent = false;
            for (int j = 0; j < GameManager.LoadInfo.username.Length; j++)
            {
                if (gamePlayerParent.transform.GetChild(i).GetComponent<GamePlayerItemList>().username == GameManager.LoadInfo.username[j])
                {
                    isPresent = true;
                    break;
                }
            }

            if (!isPresent)
            {
                GamePlayerItemList gamePlayer = getgamePlayer(gamePlayerParent.transform.GetChild(i).GetComponent<GamePlayerItemList>().username);
                gamePlayer.username = "";
                gamePlayerParent.transform.GetChild(i).gameObject.SetActive(false);
            }
        }

        cardDrawnText.text = "Cards Drawn: "+GameManager.LoadInfo.openCount;
        cardRemainingText.text = "Cards Left: " + GameManager.LoadInfo.remaining;

        if (GameManager.LoadInfo.openCount == 52)
        {
            GameManager.OnDisableNextCard();
        }
        

        if (GameManager.LoadInfo.isGameStarted)
        {
            return;
        }

        hostPlayerPrefab.SetActive(GameManager.LoadInfo.ownerName == UserData.username);
        playerPrefab.SetActive(false);
        hostGamePanel.SetActive(false);
    }

    void OpenCard()
    {
        if (!nextCard.gameObject.activeSelf)
        {
            nextCard.gameObject.SetActive(true);
        }
        print(GameManager.LoadOpenCard.openCard);
        nextCard.cardName = GameManager.LoadOpenCard.openCard;
    }

    void OnDistributeCard()
    {

        stickyButton.GetComponent<Button>().interactable = false;
        lastButton.GetComponent<Button>().interactable = false;
        string card = "";
        for (int i = 0; i < GameManager.LoadDistributeCard.username.Length; i++)
        {
            if (GameManager.LoadDistributeCard.username[i] == UserData.username)
            {
                card = GameManager.LoadDistributeCard.card[i];
                break;
            }
        }

        if (string.IsNullOrEmpty(card))
        {
            print("player card not found");
            return;
        }

        string[] splittedCard = card.Split(',');
        Dictionary<string, List<int>> sortedCards = new Dictionary<string, List<int>>();

        for (int i = 0; i < splittedCard.Length; i++)
        {
            string[] c = splittedCard[i].Split('_');
            if (!sortedCards.ContainsKey(c[1]))
            {
                List<int> temp = new List<int>();
                temp.Add(int.Parse(c[0]));
                sortedCards.Add(c[1], temp);
            }
            else
            {
                sortedCards[c[1]].Add(int.Parse(c[0]));
            }
        }

        foreach (KeyValuePair<string, List<int>> item in sortedCards)
        {
            item.Value.Sort();
        }

        foreach (KeyValuePair<string, List<int>> item in sortedCards)
        {
            for (int i = 0; i < loadedCards.Length; i++)
            {
                if (loadedCards[i].index == int.Parse(item.Key))
                {
                    int x = 0;
                    for (int j = item.Value.Count -1; j >=0; j--)
                    {
                        loadedCards[i].cardObj[x].GetComponent<CardItemList>().cardName = item.Value[j] + "_" + item.Key;
                        loadedCards[i].cardObj[x].gameObject.SetActive(true);
                        x++;
                    }
                }
            }
        }
    }


    bool isPlayerPresent(string username)
    {
        bool gamePlayer = false;

        for (int i = gamePlayerParent.transform.childCount - 1; i > 0; i--)
        {
            if (username == gamePlayerParent.transform.GetChild(i).GetComponent<GamePlayerItemList>().username)
            {
                gamePlayer = true;
                break;
            }
        }

        return gamePlayer;

    }

    GamePlayerItemList getgamePlayer(string username)
    {
        GamePlayerItemList gamePlayer = null;

        for (int i = gamePlayerParent.transform.childCount - 1; i > 0; i--)
        {
            if (username == gamePlayerParent.transform.GetChild(i).GetComponent<GamePlayerItemList>().username)
            {
                gamePlayer = gamePlayerParent.transform.GetChild(i).GetComponent<GamePlayerItemList>();
                break;
            }
        }

        return gamePlayer;

    }


}
