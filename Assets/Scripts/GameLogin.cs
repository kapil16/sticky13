﻿using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum CanvasType
{
    Login, Game
}

public class GameLogin : MonoBehaviour
{
    public InputField emailText;
    //public Text roomNameText;
    public Button setupGameButton;

    public Button joinGameButton;

    public InputField usernameText;
    public InputField roomNameText;

    public Text errorText;

    static GameLogin instance_;

    void OnEnable()
    {
        if (setupGameButton != null)
        {
            setupGameButton.onClick.AddListener(OnSetupGame);
        }

        if (joinGameButton != null)
        {
            joinGameButton.onClick.AddListener(JoinGame);
        }

        System.Guid myGUID = System.Guid.NewGuid();
        UserData.username = myGUID.ToString();
        SFS.ExtensionResponseReceived += OnExtensionResponse;
        instance_ = this;
    }

    void OnDisable()
    {
        if (setupGameButton != null)
        {
            setupGameButton.onClick.RemoveListener(OnSetupGame);
        }

        if (joinGameButton != null)
        {
            joinGameButton.onClick.RemoveListener(JoinGame);
        }

        SFS.ExtensionResponseReceived -= OnExtensionResponse;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            OnUpdateInput();
        }
    }

    void OnExtensionResponse(string cmd, SFSObject dataObject, Room room)
    {
        switch (cmd)
        {
            case "JoinTable":
                {
                    HandleJoinTable(dataObject);
                    break;
                }

            case "JoinRoom":
                {
                    HandleJoinRoom(dataObject);
                    break;
                }
            case "Login":
                {
                    HandleLogin(dataObject);
                    break;
                }
        }
    }

    void HandleLogin(SFSObject dataObject)
    {
        bool status = dataObject.GetBool("status");
        if (!status)
        {
            ShowError("Change Username");
            return;
        }
        SFS.Logout(() =>
        {
            SFS.Login(usernameText.text, "", GameStats.zoneName, () =>
            {
                UserData.username = usernameText.text;
                JoinRoom();
            }, (string error) =>
            {
                print("error : " + error);
            });
        }, (string error) =>
        {
            print("error -- > " + error);
        });

    }

    void HandleJoinTable(SFSObject dataObject)
    {
        if (dataObject.GetBool("status"))
        {
            GameStats.roomName = dataObject.GetUtfString("name");
            ApiCall.CreateRoom(GameStats.roomName, emailText.text);
        }
    }

    public static void Create()
    {
        Common.OnUpdateCanvas(CanvasType.Game);
        instance_.resetFields();
    }


    void HandleJoinRoom(SFSObject dataObject)
    {
        if (dataObject.GetBool("status"))
        {
            GameStats.roomName = roomNameText.text;
            Common.OnUpdateCanvas(CanvasType.Game);
            resetFields();

        }
    }

    void resetFields()
    {
        usernameText.text = "";
        roomNameText.text = "";
        emailText.text = "";
    }

    void OnSetupGame()
    {
        if (string.IsNullOrEmpty(emailText.text))
        {
            return;
        }

        if (string.IsNullOrEmpty(UserData.username))
        {
            Debug.Log("UserData.username : " + UserData.username);
            return;
        }

        Connect(UserData.username, true);
    }

    void JoinGame()
    {
        if (string.IsNullOrEmpty(usernameText.text))
        {
            return;
        }
        //System.Guid myGUID = System.Guid.NewGuid();
        //UserData.username = myGUID.ToString();

        Connect(UserData.username, false);
    }

    void Connect(string username, bool isCreate)
    {
        if (!SFS.connected)
        {
            SFS.Connect(() =>
            {
                SFS.Login(username, "", GameStats.zoneName, () =>
                {
                    if (isCreate)
                    {
                        CreateRoom();
                    }
                    else
                    {
                        ValidateUsername(usernameText.text);
                    }
                }, (string error) =>
                {
                    print("error : " + error);
                });
            }, (string error) =>
            {

            });
        }
        else
        {
            if (SFS.loggedIn)
            {
                if (isCreate)
                {
                    CreateRoom();
                }
                else
                {
                    ValidateUsername(usernameText.text);
                }
            }
            else
            {
                SFS.Login(username, "", GameStats.zoneName, () =>
                {
                    if (isCreate)
                    {
                        CreateRoom();
                    }
                    else
                    {
                        ValidateUsername(username);
                    }
                }, (string error) =>
                {
                    print("error : " + error);
                });
            }
        }
    }



    void CreateRoom()
    {
        SFS.SendExtensionRequest("JoinTable", new SFSObject());
    }

    void JoinRoom()
    {
        if (string.IsNullOrEmpty(roomNameText.text))
        {
            return;
        }
        ApiCall.OnJoin(roomNameText.text);
    }

    void ValidateUsername(string name)
    {
        SFSObject objOut = new SFSObject();
        objOut.PutUtfString("nickname", name);
        SFS.SendExtensionRequest("Login", objOut);
    }


    public static void Join(string roomName)
    {
        SFSObject objOut = new SFSObject();
        objOut.PutUtfString("roomName", roomName);
        SFS.SendExtensionRequest("JoinRoom", objOut);
    }


    public static void ShowError(string errorMessage)
    {
        if (!instance_.errorText.gameObject.activeSelf)
        {
            instance_.errorText.gameObject.SetActive(true);
        }
        instance_.errorText.text = errorMessage;
    }

    void OnUpdateInput()
    {
        if (!usernameText.isFocused && !roomNameText.isFocused)
        {
            return;
        }

        if (usernameText.isFocused)
        {
            roomNameText.ActivateInputField();
            usernameText.DeactivateInputField();
        }
        else
        {
            usernameText.ActivateInputField();
            roomNameText.DeactivateInputField();
        }
    }

    void OnApplicationQuit()
    {
        SFS.Disconnect(null);
    }
}
