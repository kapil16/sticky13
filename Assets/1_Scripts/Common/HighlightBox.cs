﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class HighlightBox : MonoBehaviour { 
    [System.Serializable] 
    public class Highlight {
        public string[] linesText;
        public Color[] linesColor;
        public float persistTime;
        public int importance;
        public int layoutIndex;
    }

    public Transform[] layouts;
    public Color defaultColor = Color.white;

    public List<Highlight> latestHighlights = new List<Highlight>();
    public Highlight lastHighlight;

    void OnEnable() {
        CloseAllLayouts();
    }

    public Highlight Add(string text, Color color, float persistTime = 5, int importance = 5, int layoutIndex = 0)
    {
        return Add(new string[1] {text}, new Color[1] {color}, persistTime, importance, layoutIndex);
    }

    public Highlight Add(string line1text, Color line1color, string line2text, Color line2color, float persistTime = 5, int importance = 5, int layoutIndex = 0)
    {
        return Add(new string[2] { line1text, line2text }, new Color[2] { line1color, line2color }, persistTime, importance, layoutIndex);
    }

    public Highlight Add(string line1text, Color line1color, string line2text, Color line2color, string line3text, Color line3color, float persistTime = 5, int importance = 5, int layoutIndex = 0)
    {
        return Add(new string[3] { line1text, line2text, line3text }, new Color[3] { line1color, line2color, line3color }, persistTime, importance, layoutIndex);
    }

    public Highlight Add(string[] linesText, Color[] linesColor, float persistTime=5, int importance=5, int layoutIndex=0) {
        Highlight h = new Highlight();
        h.linesText = linesText;
        if (linesColor == null) linesColor = new Color[1] {defaultColor};
        if (linesColor.Length<linesText.Length)
        {
            h.linesColor = new Color[linesText.Length];
            for (int i = 0; i < h.linesColor.Length; i++)
            {
                if (i < linesColor.Length) h.linesColor[i] = linesColor[i];
                else h.linesColor[i] = defaultColor;
            }
        }
        else h.linesColor = linesColor;
        h.persistTime = persistTime;
        h.importance = importance;
        h.layoutIndex = Mathf.Clamp(layoutIndex, 0, layouts.Length - 1);
        latestHighlights.Add(h);
        ShowNew();
        return h;
    }

    void ShowNew() {
        if (latestHighlights.Count == 0) return;
        Highlight contendor = latestHighlights[latestHighlights.Count - 1];
        if (lastHighlight!=null)
        {
            if (contendor == lastHighlight) return;
            if (contendor.importance < lastHighlight.importance) return;
        }

        string s = "";
        for (int i = 0; i < contendor.linesText.Length; i++)
        {
            if (i != 0) s += "\n";
            s += "[" + contendor.linesColor[i].ToHex() + "]";
            s += contendor.linesText[i];
        }
        layouts[contendor.layoutIndex].GetChild(1).GetComponent<Text>().text = s;
        layouts[contendor.layoutIndex].GetChild(1).GetComponent<Text>().color = contendor.linesColor[0];
        lastHighlight = contendor;
        
        CloseAllLayouts();
        layouts[contendor.layoutIndex].gameObject.SetActive(true); 
        if (contendor.persistTime>0) StartCoroutine(AutoRemove(contendor, contendor.persistTime));
    }  

    void Remove(Highlight h)
    {
        if (latestHighlights.Contains(h)) latestHighlights.Remove(h);
        if (lastHighlight != null && lastHighlight == h)
        {
            layouts[lastHighlight.layoutIndex].gameObject.SetActive(false);
            lastHighlight = null;
        }
        if (latestHighlights.Count > 0) ShowNew();
    }

    IEnumerator AutoRemove(Highlight h, float t) {
        yield return new WaitForSeconds(t);
        Remove(h);
    }

    void CloseAllLayouts() {
        for (int i = 0; i < layouts.Length; i++) layouts[i].gameObject.SetActive(false);
    }
}
