﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DisconnectionHandler : MonoBehaviour {
    
	void OnEnable ()
    {
        SFS.ConnectionToServerLost += OnConnectionLost;
        StartCoroutine(DelayedOnEnable());
    }

    IEnumerator DelayedOnEnable()
    {
        yield return null; 
        Check();
    }
	
	void OnDisable ()
    {
        SFS.ConnectionToServerLost -= OnConnectionLost;
    }

    void OnApplicationPause(bool paused) {
        if (!paused) Check(); 
    }

    bool Check()
    {
        if (!SFS.connected) OnConnectionLost("UNKNOWN"); 
        return SFS.connected;
    }

    void OnConnectionLost(string error)
    {
        StartCoroutine(OnConnectionLost_c(error));
    }
    IEnumerator OnConnectionLost_c(string error)
    {
      
        yield break;
    }
}
