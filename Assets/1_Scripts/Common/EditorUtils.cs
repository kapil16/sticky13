﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;

public class EditorUtils {
    
}

public class FileUtils
{
    public static void ModifyText(string path, string tag, char startChar, char endChar, string stringToInsert)
    {
        StringBuilder newScript = new StringBuilder();

        using (StreamReader streamReader = new StreamReader(path))
        {
            string line;
            while ((line = streamReader.ReadLine()) != null)
            {
                if (line.Contains(tag))
                { 
                    //Debug.Log(line);
                    int si = line.IndexOf(startChar), ei = line.IndexOf(endChar);
                    line = line.Remove(si + 1, ei - si - 1);
                    line = line.Insert(si + 1, stringToInsert);
                    //Debug.Log(line);
                }
                newScript.AppendLine(line);
                //Debug.Log(line); 
            }
            streamReader.Close();
        }
        //Debug.Log(newScript);
        using (FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.ReadWrite))
        {
            StreamWriter streamWriter = new StreamWriter(fileStream);
            streamWriter.Write(newScript);
            streamWriter.Close();
            fileStream.Close();
        } 
    }

    public static string FindText(string path, string tag, char startChar, char endChar)
    {
        string s = "";

        using (StreamReader streamReader = new StreamReader(path))
        {
            string line;
            while ((line = streamReader.ReadLine()) != null)
            {
                if (line.Contains(tag))
                {
                    int si = line.IndexOf(startChar), ei = line.IndexOf(endChar);
                    for (int i = si+1; i < ei; i++)
                    {
                        s += line[i];
                    }
                }
            }
            streamReader.Close();
        }

        //Debug.Log(s);
        return s;
    }

    public static void ForceCompileScripts() {
        //AssetDatabase.ImportAsset(assetPath, ImportAssetOptions.ForceSynchronousImport);
        MonoScript cMonoScript = MonoImporter.GetAllRuntimeMonoScripts()[0];
        MonoImporter.SetExecutionOrder(cMonoScript, MonoImporter.GetExecutionOrder(cMonoScript));
    }
}
#endif