﻿using UnityEngine;
using System.Collections;

public class GameObjectExt : MonoBehaviour 
{
    public void ToggleActive()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }

    public void CopyPositionInvertedX(Transform refTransform)
    {
        transform.localPosition = new Vector3(-refTransform.localPosition.x, refTransform.localPosition.y, refTransform.localPosition.z);
    }

    public void ResetLocalPosition()
    {
        transform.localPosition = Vector3.zero;
    }
    public void ResetLocalPositionDelayed()
    {
        new Delayed.Action(ResetLocalPosition, .1f);
    }
}
