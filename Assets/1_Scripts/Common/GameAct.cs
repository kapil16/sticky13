﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
#endif

[AddComponentMenu("Generic Scripts/Act")]
public class GameAct : MonoBehaviour {
    [System.Serializable]
    public class GA_Function
    {
        public UnityEvent function;
        public float delay;
        public GameObject reqActiveGo;
    }
    [System.Serializable]
    public class GA_Sound { 
        public AudioClip audio;
        public float delay;
        public float volume=1;
    }
    [System.Serializable]
    public class GA_Animation
    {
        public AnimationClip clip;
        public float delay;
        //public float speedX=1;
    }
    [System.Serializable]
    public class GA_ObjectState
    {
        public GameObject gameObject;
        public bool state;
        public float delay;
    }

    [HideInInspector] public bool loop;
    [HideInInspector] public bool startOnEnable;
    [HideInInspector] public int partsRunning; 
    [Space] public List<GA_Function> functions = new List<GA_Function>();
    [HideInInspector] public List<GA_ObjectState> gameObjects = new List<GA_ObjectState>();
    [HideInInspector] public Animation animationComp;
    [HideInInspector] public List<GA_Animation> animations = new List<GA_Animation>();
    [HideInInspector] public AudioSource audioSource; 
    [HideInInspector] public List<GA_Sound> sounds = new List<GA_Sound>();

    [HideInInspector] public bool objectsEx, animsEx, soundsEx;

    void OnEnable() {
        if (startOnEnable) StartAct();
    }
    void OnDisable()
    {
        StopAct();
    }

    public void StartAct() {
        if (partsRunning != 0) StopAct();
        if (gameObject.activeSelf)
        {
            if (functions.Count > 0) StartCoroutine("InvokeFunctions");
            if (gameObjects.Count > 0) StartCoroutine("InvokeObjectsState");
            if (animations.Count > 0 && animationComp != null) StartCoroutine("InvokeAnimations");
            if (sounds.Count > 0 && audioSource != null) StartCoroutine("InvokeSounds");
        }
    }

    public void StopAct()
    {
        if (partsRunning == 0) return;

        StopCoroutine("InvokeFunctions");
        StopCoroutine("InvokeObjectsState");
        StopCoroutine("InvokeAnimations");
        StopCoroutine("InvokeSounds");
        if (audioSource != null) audioSource.Stop();

        partsRunning = 0;
    }

    IEnumerator InvokeFunctions()
    {
        partRunning(true);
        for (int i = 0; i < functions.Count; i++)
        {
            if(functions[i].delay>0) yield return new WaitForSeconds(functions[i].delay);
            if (functions[i].reqActiveGo!=null)
            {
                if (functions[i].reqActiveGo.activeSelf) functions[i].function.Invoke();
            }
            else functions[i].function.Invoke();
        }
        partRunning(false);
    }
    IEnumerator InvokeObjectsState()
    {
        partRunning(true);
        for (int i = 0; i < gameObjects.Count; i++)
        {
            if (gameObjects[i].delay > 0) yield return new WaitForSeconds(gameObjects[i].delay);

            if (gameObjects[i].gameObject != null) gameObjects[i].gameObject.SetActive(gameObjects[i].state);
            else Debug.LogError("Unassigned reference: GameObject of " + i.ToString(), this);
        }
        partRunning(false);
    }
    IEnumerator InvokeAnimations()
    {
        partRunning(true);
        for (int i = 0; i < animations.Count; i++)
        {
            if (animations[i].delay > 0) yield return new WaitForSeconds(animations[i].delay);
            //animationComp[animations[i].clip.name].speed = animations[i].speedX;
            //animationComp[animations[i].clip.name].time = animationComp[animations[i].clip.name].length;
            if (animationComp != null)
            {
                if (animations[i].clip != null)
                {
                    animationComp.Play(animations[i].clip.name);
                }
                else Debug.LogError("Unassigned reference: Animation Clip at " + i.ToString(), this);
            }
            else Debug.LogError("Unassigned reference: Animation Component", this);
            yield return new WaitForSeconds(animationComp[animations[i].clip.name].length); 
        }
        partRunning(false);
    }
    IEnumerator InvokeSounds()
    {
        partRunning(true);
        for (int i = 0; i < sounds.Count; i++)
        {
            if (sounds[i].delay > 0) yield return new WaitForSeconds(sounds[i].delay);


            if (audioSource != null)
            {
                if (sounds[i].audio != null)
                {
                    audioSource.PlayOneShot(sounds[i].audio, sounds[i].volume);
                }
                else Debug.LogError("Unassigned reference: Audio Clip at " + i.ToString(), this);
            }
            else Debug.LogError("Unassigned reference: Audio Source", this);

        }
        partRunning(false);
    }

    void partRunning(bool add) {
        if (add) partsRunning++;
        else partsRunning--;
        //print("partRunning " + add + " " + partsRunning);
        if (partsRunning == 0 && loop) StartAct();
    }

    public float getRunningTime() {
        float t = 0,tt=0;

        for (int i = 0; i < functions.Count; i++)
        {
            tt += functions[i].delay;
        }
        if (tt > t) t = tt;
        tt = 0;

        for (int i = 0; i < gameObjects.Count; i++)
        {
            tt += gameObjects[i].delay;
        }
        if (tt > t) t = tt;
        tt = 0;

        for (int i = 0; i < animations.Count; i++)
        {
            tt += animations[i].delay;
            tt += Mathf.Abs(animationComp[animations[i].clip.name].length);
        }
        if (tt > t) t = tt;
        tt = 0;

        for (int i = 0; i < sounds.Count; i++)
        {
            tt += sounds[i].delay;
        }
        if (tt > t) t = tt;
        tt = 0;

        //print(t);
        return t;
    }
}

#region editor
#if UNITY_EDITOR  
[CustomEditor(typeof(GameAct))]
public class GameActEditor : Editor
{
    public GameAct script;
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        script = target as GameAct;

        //GUILayout.HorizontalSlider(1, 0, 1, GUILayout.Width(2000));
        EditorGUILayout.LabelField("________________________________________________");
        //EditorGUILayout.LabelField("__________________ __ _");

        EditorGUILayout.Space(); 
        EditorGUILayout.BeginHorizontal(); 
        //if (GUILayout.Button(script.objectsEx?"^":">", GUILayout.Width(20))) script.objectsEx = !script.objectsEx;
        GUILayout.Label("Objects");
        if (GUILayout.Button("+", GUILayout.Width(50))) script.gameObjects.Add(new GameAct.GA_ObjectState());
        EditorGUILayout.EndHorizontal();

        //if (script.objectsEx)
        //{
        if (script.gameObjects.Count > 0)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Game Object");
            EditorGUILayout.LabelField("Set", GUILayout.Width(30));
            EditorGUILayout.LabelField("Delay", GUILayout.Width(50));
            EditorGUILayout.LabelField("Rearrange", GUILayout.Width(70));
            EditorGUILayout.EndHorizontal();

            for (int i = 0; i < script.gameObjects.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                script.gameObjects[i].gameObject = (GameObject)EditorGUILayout.ObjectField(script.gameObjects[i].gameObject, typeof(GameObject), true);
                script.gameObjects[i].state = EditorGUILayout.Toggle(script.gameObjects[i].state, GUILayout.Width(30));
                script.gameObjects[i].delay = EditorGUILayout.FloatField(script.gameObjects[i].delay, GUILayout.Width(50));
				if (GUILayout.Button("-", GUILayout.Width(20))) script.gameObjects.RemoveAt(i);

				if (i > 0) {
					if (GUILayout.Button ("˄", GUILayout.Width (20))) {
						script.gameObjects.Insert ((i - 1), script.gameObjects [i]);
						script.gameObjects.RemoveAt ((i + 1));
					}
				} 
				else GUILayout.Label ("", GUILayout.Width (20));

				if (i < script.gameObjects.Count-1) {
					if (GUILayout.Button ("˅", GUILayout.Width (20))) {
						script.gameObjects.Insert ((i+2), script.gameObjects [i]);
						script.gameObjects.RemoveAt ((i));
					}
				}
				else GUILayout.Label ("", GUILayout.Width (20));
                EditorGUILayout.EndHorizontal();
            }
        }
        //}

        //GUILayout.HorizontalSlider(1, 0, 1, GUILayout.Width(2000));
        EditorGUILayout.LabelField("________________________________________________");

        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("Animations"); 
        if (GUILayout.Button("+", GUILayout.Width(50))) script.animations.Add(new GameAct.GA_Animation());
        EditorGUILayout.EndHorizontal();

        if (script.animations.Count > 0)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Clip");
            //EditorGUILayout.LabelField("Speed", GUILayout.Width(50));
            EditorGUILayout.LabelField("Delay", GUILayout.Width(50));
            EditorGUILayout.LabelField("Remove", GUILayout.Width(50));
            EditorGUILayout.EndHorizontal();

            for (int i = 0; i < script.animations.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                script.animations[i].clip = (AnimationClip)EditorGUILayout.ObjectField(script.animations[i].clip, typeof(AnimationClip), true);
                //script.animations[i].speedX = EditorGUILayout.FloatField(script.animations[i].speedX, GUILayout.Width(50));
                script.animations[i].delay = EditorGUILayout.FloatField(script.animations[i].delay, GUILayout.Width(50));
                if (GUILayout.Button("-", GUILayout.Width(50))) script.animations.RemoveAt(i);
                EditorGUILayout.EndHorizontal();
            }

            script.animationComp = (Animation)EditorGUILayout.ObjectField("Component", script.animationComp, typeof(Animation), true);
        }

        //GUILayout.HorizontalSlider(1, 0, 1, GUILayout.Width(2000));
        EditorGUILayout.LabelField("________________________________________________");

        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("Sounds");
        if (GUILayout.Button("+", GUILayout.Width(50))) script.sounds.Add(new GameAct.GA_Sound());
        EditorGUILayout.EndHorizontal();

        if (script.sounds.Count>0)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Clip");
            EditorGUILayout.LabelField("Volume", GUILayout.Width(50));
            EditorGUILayout.LabelField("Delay", GUILayout.Width(50));
            EditorGUILayout.LabelField("Remove", GUILayout.Width(50));
            EditorGUILayout.EndHorizontal();

            for (int i = 0; i < script.sounds.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                script.sounds[i].audio = (AudioClip)EditorGUILayout.ObjectField(script.sounds[i].audio, typeof(AudioClip), true);
                script.sounds[i].volume = EditorGUILayout.FloatField(script.sounds[i].volume, GUILayout.Width(50));
                script.sounds[i].delay = EditorGUILayout.FloatField(script.sounds[i].delay, GUILayout.Width(50));
                if (GUILayout.Button("-", GUILayout.Width(50))) script.sounds.RemoveAt(i);
                EditorGUILayout.EndHorizontal();
            }

            script.audioSource = (AudioSource)EditorGUILayout.ObjectField("Source", script.audioSource, typeof(AudioSource), true);
        }

        //GUILayout.HorizontalSlider(1, 0, 1, GUILayout.Width(2000));
        EditorGUILayout.LabelField("________________________________________________");

        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.BeginVertical();
        script.loop = EditorGUILayout.Toggle("Loop", script.loop);
        script.startOnEnable = EditorGUILayout.Toggle("Start On Enable", script.startOnEnable);
        EditorGUILayout.EndVertical();
        if (GUILayout.Button("Start", GUILayout.Width(50), GUILayout.Height(30))) script.StartAct();
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();

    }
}
#endif
#endregion