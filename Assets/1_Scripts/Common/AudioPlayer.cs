﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class AudioPlayer : MonoBehaviour {
	[System.Serializable]
	public class Audio{
		public string key;
		public AudioClip clip; 
	}

    public static AudioPlayer _instance;
    public static AudioPlayer instance {
        get {
            if (_instance==null)
            {
                _instance = FindObjectOfType<AudioPlayer>();
                _instance.Initialise();
            }
            return _instance;
        }
        set {
            _instance = value;
        }
    }

    public List<Audio> audios;
    static Dictionary<string, Audio> _audios;
    public static Dictionary<string, Audio> Audios
    {
        get
        {
            if (_audios==null)
            {
                _audios = new Dictionary<string, Audio>();
                for (int i = 0; i < instance.audios.Count; i++) if (!_audios.ContainsKey(instance.audios[i].key)) Audios.Add(instance.audios[i].key, instance.audios[i]);  
            }
            return _audios;
        }    
    }

    [Space]
    public AudioSource fxSource;
    public AudioSource musicSource; 

    [Space]
    public bool playerReady = false;
    public bool effectsOn = true, musicOn = true;  

	void OnEnable()
    {
        Initialise();
    }
     
    void Initialise()
    {
        if (playerReady) return; 

        if (PlayerPrefs.HasKey("soundEffectOff")) effectsOn = false;
        else effectsOn = true;
        SetFx(effectsOn);

        if (PlayerPrefs.HasKey("musicOff")) musicOn = false;
        else musicOn = true;
        SetMusic(musicOn);

        instance = this;
        playerReady = true;
    }


    public static bool isEffectsOn()
    {
        return instance.effectsOn;
    }


    public static bool isMusicOn()
    {
        return instance.musicOn;
    }

    void OnDisable(){ 
        _audios = null;
        //print("AP Destroyed: " + transform.name);
    }  

	public static void PlaySFX(string key, float delay=0)
    {
        if (string.IsNullOrEmpty(key) || !Audios.ContainsKey(key)) return;
        instance.StartCoroutine(instance.play_c (Audios[key].clip, delay));
	}
    public static void PlaySFX(AudioClip clip, float delay = 0)
    { 
        instance.StartCoroutine(instance.play_c(clip, delay));
    }
    IEnumerator play_c(AudioClip clip, float delay)
    {
        if (!instance.playerReady) yield break;
        if (!instance.effectsOn) yield break;
        if (delay>0)yield return new WaitForSeconds (delay);
        fxSource.PlayOneShot(clip);
#if UNITY_EDITOR
        Visualize(clip.name);
#endif
        //Debug.Log(key+" "+useUnityPlayer.ToString(), gameObject);
    }

	public static void SwitchFx(){
        instance.SwitchFxIns();
    }
    public static void SetFx(bool on)
    {
        instance.SetFxIns(on);
    }
    public void SwitchFxIns()
    {
        instance.SetFxIns(!effectsOn);
    }
    public void SetFxIns(bool on)
    {
        if (!on)
        {
            fxSource.volume = 0;
            effectsOn = false;
            PlayerPrefs.SetInt("soundEffectOff", 1);
        }
        else
        {
            fxSource.volume = 1;
            effectsOn = true;
            PlayerPrefs.DeleteKey("soundEffectOff");
        }
        float o = on ? 1 : .25f;
    }


    public static void PlayMusic(string key)
    {
        instance.PlayMusicIns(Audios[key].clip);
    }

    public static void PlayMusic(AudioClip clip)
    {
		instance.PlayMusicIns(clip);
    }
	public void PlayMusicIns(AudioClip clip)
    {
        if (!musicOn) return;
        //if (musicSource.gameObject.activeSelf) return;
		musicSource.clip = clip;
        musicSource.gameObject.SetActive(true);
        musicSource.Play();
    }
    public static void StopMusic()
    {
        instance.StopMusicIns();
    }
    public void StopMusicIns()
    {
        if (!musicOn) return;
        musicSource.gameObject.SetActive(false);
    }

    public static void SwitchMusic()
    {
        instance.SwitchMusicIns();
    }
    public static void SetMusic(bool on)
    {
        instance.SetMusicIns(on);
    }
    public void SwitchMusicIns()
    {
        SetMusicIns(!musicOn);
    }
    public void SetMusicIns(bool on)
    {
        if (on)
        {
//          musicSource.volume = 1;
            musicSource.mute = false;
            musicOn = true;
            PlayerPrefs.DeleteKey("musicOff");
        }
        else
        {
            //musicSource.volume = 0;
            musicSource.mute = true;
            musicOn = false;
            PlayerPrefs.SetInt("musicOff", 1);
        }
        musicSource.gameObject.SetActive(on);
        float o = on ? 1 : .25f;
    }

    public Text visualizeTxtComp;
    void Visualize(string msg) {
        if (visualizeTxtComp!=null)
        {
            visualizeTxtComp.transform.parent.gameObject.SetActive(false);
            visualizeTxtComp.transform.parent.gameObject.SetActive(true);
            visualizeTxtComp.text = msg;
        }
    }
}
