﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ChildrenRename : MonoBehaviour {

	public string format;

	public void Rename(){
		for (int i = 0; i < transform.childCount; i++) {
			transform.GetChild (i).gameObject.name = string.Format (format, transform.GetChild (i).gameObject.name, i.ToString());
		}
	}

}


#if UNITY_EDITOR
[CustomEditor(typeof(ChildrenRename))]
public class ChildrenRenameEditor : Editor{
	public ChildrenRename script;
	void OnEnable()
	{
		script = target as ChildrenRename;
	}

	public override void OnInspectorGUI ()
	{
		base.OnInspectorGUI ();
		EditorGUILayout.HelpBox ("{0} is previous name, {1} is index", MessageType.Info);
		if (GUILayout.Button("Rename")) {
			script.Rename ();
		}
	}
}
#endif