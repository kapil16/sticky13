﻿using UnityEngine;
using System.Collections;
using System.Net;

public class DeviceDetails : MonoBehaviour {
	

	public static string DeviceIP{
//		get{ return Network.player.ipAddress;}
		get{string hostName = Dns.GetHostName ();
			var addresses = Dns.GetHostEntry (hostName).AddressList;
			return  addresses [addresses.Length - 1].ToString ();}
	}

	public static string DeviceType{
		get{
			string DeviceType = "";
			#if UNITY_EDITOR 
			DeviceType = "UnityEditor";
			#elif UNITY_STANDALONE_WIN
			DeviceType = "WindowsPC";
			#elif UNITY_WSA
			DeviceType = "Windows10Device";
			#elif UNITY_STANDALONE_OSX
			DeviceType = "MacPC";
			#elif UNITY_STANDALONE_LINUX
			DeviceType = "LinuxPC";
			#elif UNITY_ANDROID
			DeviceType = "Android";
			#elif UNITY_IOS
			DeviceType = "iPhone";
			#endif
			return DeviceType;
		}
	}

	public static string AndroidIEMI{
		get{
				AndroidJavaClass up = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
				AndroidJavaObject currentActivity = up.GetStatic<AndroidJavaObject> ("currentActivity");
				AndroidJavaObject contentResolver = currentActivity.Call<AndroidJavaObject> ("getContentResolver");  
				AndroidJavaClass secure = new AndroidJavaClass ("android.provider.Settings$Secure");
				string android_id = secure.CallStatic<string> ("getString", contentResolver, "android_id");
				AndroidJavaObject TM = new AndroidJavaObject("android.telephony.TelephonyManager");
				string IMEI = TM.Call<string>("getDeviceId");
				return IMEI;
			}
	}

}
