﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AudioToggle : MonoBehaviour 
{
    Toggle toggle;
    public bool toggleSFX;
    public bool toggleMusic;

    void OnEnable()
    {
        if (toggle==null)
        {
            toggle = GetComponent<Toggle>();
            toggle.onValueChanged.AddListener(OnToggle);
        }
        if (toggleSFX)
        {
            toggle.isOn = AudioPlayer.instance.effectsOn;
        }
        if (toggleMusic)
        {
            toggle.isOn = AudioPlayer.instance.musicOn;
        }
    }

    void OnToggle(bool b)
    { 
        if (toggleSFX)
        {
            AudioPlayer.SetFx(b);
        }
        if (toggleMusic)
        { 
            AudioPlayer.SetMusic(b);
        }
    }
}
