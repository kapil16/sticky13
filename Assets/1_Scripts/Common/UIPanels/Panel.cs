﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Panel : MonoBehaviour 
{ 
    public PanelGroup group;
    [HideInInspector]public bool isActive;

    public event System.Action Activated,Deactivated;

    void Awake()
    { 
        if (group!=null)
        {
            group.RegisterPanel(this); 
        }
        isActive = true;
    }

    public void Toggle()
    {
        Toggle(!isActive);
    } 

    public void Toggle(bool _activate)
    {
        if (_activate) Activate();
        else Deactivate();
    }

    public void Activate()
    {
        if (isActive) return;
        isActive = true;
        gameObject.SetActive(true);
        if (group != null) group.OnPanelActivate(this);
        if (Activated != null) Activated();
    }

    public void Deactivate()
    {
        if (!isActive) return;
        isActive = false;
        gameObject.SetActive(false);
        if (group != null) group.OnPanelDeactivate(this);
        if (Deactivated != null) Deactivated();
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(Panel))]
public class PanelEditor : Editor
{
    Panel script;
    void OnEnable()
    {
        script = target as Panel; 
        Registger();
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Activate"))
        {
            script.Activate();
        }
        if (GUILayout.Button("Deactivate"))
        {
            script.Deactivate();
        }
        EditorGUILayout.EndHorizontal();

        if (GUI.changed)
        {
            Registger();
        }
    }

    void Registger()
    {
        if(script.group != null) script.group.RegisterPanel(script); 
    }
}
#endif