﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using System.IO;

public class Localization : MonoBehaviour {
    [System.Serializable]
    public class LanguageData
    {
        [HideInInspector]
        public string key;
        public int rev;
        public TextAsset textAsset;
        public string textFileLocation; 
        public JSONNode strings;
    }

    static bool debug = false;
    public static Localization instance;
    public static Dictionary<string, LanguageData> languagesDictionary = new Dictionary<string, LanguageData>();
    public static string currentLanguageKey="",fallbackLanguageKey="En-IN";
    public static event System.Action CurrentLanguageUpdated;
    public static event System.Action NewLanguageAdded;

    public List<LanguageData> bundledLanguagesData = new List<LanguageData>();
    public string downloadedLanguagesLocation {
        get { 
            return Application.persistentDataPath + "/Localization/";
        }
    }
    public string downloadableLanguagesInfoReqUrl
    {
        get
        {
            return "file:///F:/Girish/TeamPoker/Projects/Repo/Assets/1_Scripts/Common/Localization/dummyLangInfoJson.txt";
        }
    }
    

    void OnEnable() {
        if (instance != null) Destroy(gameObject);
        instance = this;
        DontDestroyOnLoad(gameObject);
        Initialize();
    }

    static bool initialized;
    void Initialize() {
        if (initialized) return;

        AddBundledLanguages();
        AddDownloadedLanguages();
        GetDownloadableLanguages();

        initialized = true;
    }

    void AddBundledLanguages() {
        if(debug) Debug.Log("Adding bundled languages");
        for (int i = 0; i < bundledLanguagesData.Count; i++) AddNewLanguage(bundledLanguagesData[i]);
    }

    void AddDownloadedLanguages()
    {
        if(debug) Debug.Log("Adding downloaded languages");
        if (!Directory.Exists(downloadedLanguagesLocation)) Directory.CreateDirectory(downloadedLanguagesLocation);

        string[] filesLocation = Directory.GetFiles(downloadedLanguagesLocation);

        for (int i = 0; i < filesLocation.Length; i++) AddDownloadedLanguage(filesLocation[i]);
    }

    void AddDownloadedLanguage(string fileLocation)
    {
        if(debug) Debug.Log("Adding downloaded language from " + fileLocation);
        LanguageData data = new LanguageData();
        data.textFileLocation = fileLocation; 
        AddNewLanguage(data);
    }

    void GetDownloadableLanguages()
    {
        if(debug) Debug.Log("Getting downloadable languages");
        new RestAPI.Get(downloadableLanguagesInfoReqUrl, DownloadNewLanguages, Debug.LogError);
    }
    
    void DownloadNewLanguages(string langugesInfoJson)
    {
        if(debug) Debug.Log("downloading new languages");
        JSONNode langugesInfoJsonNode = JSON.Parse(langugesInfoJson);
        //print(langugesInfoJsonNode);
        string k = "";
        for (int i = 0; i < langugesInfoJsonNode.Count; i++)
        {
            k = langugesInfoJsonNode[i]["Lang_Key"];

            if (languagesDictionary.ContainsKey(k) && languagesDictionary[k].rev > int.Parse(langugesInfoJsonNode[i]["Lang_Rev"]))
                continue;

            new RestAPI.Get(langugesInfoJsonNode[i]["Lang_Url"], OnNewLanguageDownloaded, Debug.LogError);
        }
    }

    void OnNewLanguageDownloaded(string stringsJson)
    {
        if(debug) Debug.Log("downloaded new language");
        string filePath = downloadedLanguagesLocation + Random.Range(1000,9999).ToString() + ".lang";
        FileStream fileStream = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite);
        StreamWriter streamWriter = new StreamWriter(fileStream);
        streamWriter.Write(stringsJson);
        streamWriter.Close();
        fileStream.Close();

        Delayed.Function(AddDownloadedLanguage, filePath, 1);
        if(debug) Debug.Log("language file saved at "+ filePath);
    }

    public void AddNewLanguage(LanguageData data)
    {
        if(debug) Debug.Log("Adding new language from " + (data.textAsset==null?"":"textAsset") + (string.IsNullOrEmpty(data.textFileLocation)? "" : data.textFileLocation));
        if (data.textAsset != null) data.strings = JSON.Parse(data.textAsset.text);
        else if (!string.IsNullOrEmpty(data.textFileLocation))
        {
            //print(filesLocation[i]);
            StreamReader streamReader = new StreamReader(data.textFileLocation);
            string s = streamReader.ReadToEnd();
            streamReader.Close();
            data.strings = JSON.Parse(s);
            //print("language json: \n" + s);
        }

        //print("words in language" + data.strings.Count);
        if (data.strings.Count == 0)
        {
            if(debug) Debug.Log("Failed to add the language");
            return;
        }

        data.key = data.strings["Lang_Key"];
        data.rev = int.Parse(data.strings["Lang_Rev"]);

        if (languagesDictionary.ContainsKey(data.key)) languagesDictionary.Remove(data.key);
        languagesDictionary.Add(data.key, data);
        if (NewLanguageAdded != null) NewLanguageAdded();
        if(debug) Debug.Log("Language Added: " + data.key);

        if (string.IsNullOrEmpty(currentLanguageKey) && fallbackLanguageKey == data.key) SetCurrentLanguage(data.key);
        if (currentLanguageKey == data.key) SetCurrentLanguage(data.key);
    }

    public static void SetCurrentLanguage(string key) { 
        currentLanguageKey = key;
        if (CurrentLanguageUpdated != null) CurrentLanguageUpdated();
        if(debug) Debug.Log("Language Changed: " + key);
    }

    public static string GetString(string key) { 
        if (languagesDictionary.ContainsKey(currentLanguageKey))
            return languagesDictionary[currentLanguageKey].strings[key];

        if (languagesDictionary.ContainsKey(fallbackLanguageKey))
            return languagesDictionary[fallbackLanguageKey].strings[key];

        return key;
    }

#if UNITY_EDITOR
    //void Update() {
    //    if (Input.GetKeyDown(KeyCode.W))
    //    {
    //        string key = "hello";
    //        print(GetString(key));
    //        print(GetString(key).ToUpper());
    //        print(GetString(key).ToLower());
    //        print(FormattedString.ToSentenceCase(GetString(key)));
    //    }
    //}
#endif
}  
