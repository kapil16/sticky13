﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Dropdown))]
public class LocalizationDropDown : MonoBehaviour {
    Dropdown ddComp;

    void OnEnable()
    {
        if (ddComp == null) ddComp = GetComponent<Dropdown>(); 
        Localization.NewLanguageAdded += UpdateDD;
        ddComp.onValueChanged.AddListener(ddValueChanged);
        UpdateDD();
    }

    void OnDisable()
    {
        Localization.NewLanguageAdded -= UpdateDD;
        ddComp.onValueChanged.RemoveAllListeners();
    }

    void UpdateDD()
    {
        ddComp.ClearOptions();
        List<string> languageNames = new List<string>();
        foreach (var item in Localization.languagesDictionary)
        {
            languageNames.Add(item.Value.key);
        }
        ddComp.AddOptions(languageNames);
        for (int i = 0; i < ddComp.options.Count; i++)
        {
            if(ddComp.options[i].text.Equals(Localization.currentLanguageKey))
                ddComp.value = i;
        }
    }

    public void ddValueChanged(int val) {
        Localization.SetCurrentLanguage(ddComp.options[ddComp.value].text);
    }
}
