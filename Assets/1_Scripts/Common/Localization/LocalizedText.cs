﻿using UnityEngine; 
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour {
    public Text textComp;
    public string key; 

    void OnEnable() {
        if (textComp == null) textComp = GetComponent<Text>();
        if (textComp == null) return;
        Localization.CurrentLanguageUpdated += UpdateText;
        UpdateText();
    }

    void OnDisable()
    {
        Localization.CurrentLanguageUpdated -= UpdateText;
    }

    void UpdateText() {
        if(!string.IsNullOrEmpty(key)) textComp.text = Localization.GetString(key);
    }
}
