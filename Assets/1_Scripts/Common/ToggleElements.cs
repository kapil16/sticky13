﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ToggleElements : MonoBehaviour {
	[System.Serializable]
	public class ToggleElement
	{
		public Toggle toggle;
		public GameObject element;

		public void OnValChanged(bool val)
		{
			element.SetActive (false);
			element.SetActive (val);
		}
	}

	public ToggleElement[] toggleElements;
	void OnEnable()
	{
		for (int i = 0; i < toggleElements.Length; i++) {
			toggleElements [i].toggle.onValueChanged.AddListener (toggleElements [i].OnValChanged);
		}
	}
}
