﻿using UnityEngine;
using System.Collections;

public class AudioLoop : MonoBehaviour {
	public AudioSource source;
	public float gap;

	void OnEnable(){
		StartCoroutine ("Loop");	
	}

	IEnumerator Loop(){
		while (true) {
			source.PlayOneShot (source.clip);
			yield return new WaitForSeconds (gap);
		}
	}
}
