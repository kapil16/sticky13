﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EndlessScroll : MonoBehaviour {

    Transform[] children;
    int currentChild=0, nextChild=1;
    public float speed = 50f;
    public bool randomize;

	void OnEnable () {
        children = new Transform[transform.childCount];
        for (int i = 0; i < children.Length; i++) children[i] = transform.GetChild(i); 
        Scroll();
    }
     
    void Scroll()
    {
        if (children.Length < 2) return;
        if (randomize)
        {
            List<Transform> tempList = new List<Transform>();
            int r = 0;
            tempList.AddRange(children);
            for (int i = 0; i < children.Length; i++)
            {
                r = Random.Range(0, tempList.Count);
                children[i] = tempList[r];
                tempList.RemoveAt(r);
            }
        }
        StartCoroutine(Scroll_c());
    }

    IEnumerator Scroll_c(){ 
        float heightCurrent = children[currentChild].GetComponent<RectTransform>().rect.height;
        float heightNext = children[nextChild].GetComponent<RectTransform>().rect.height;
        float distanceToTravel = heightNext;
        float distanceTravelled = 0;
        float distanceTravelDelta = 0;

        for (int i = 0; i < children.Length; i++) children[i].gameObject.SetActive(false);
        children[currentChild].localPosition = new Vector3(0, -heightCurrent / 2, 0);
        children[nextChild].localPosition = new Vector3(0, heightNext / 2, 0);
        children[currentChild].gameObject.SetActive(true);
        children[nextChild].gameObject.SetActive(true);

        while (distanceTravelled<distanceToTravel)
        {
            //print(distanceToTravel + " " + distanceTravelled);
            distanceTravelDelta = Time.deltaTime * speed;
            children[currentChild].localPosition -= new Vector3(0, distanceTravelDelta, 0);
            children[nextChild].localPosition -= new Vector3(0, distanceTravelDelta, 0);
            distanceTravelled += distanceTravelDelta;
            yield return null;
        }

        currentChild++;
        if (currentChild > children.Length - 1) currentChild = 0;
        nextChild++;
        if (nextChild > children.Length - 1) nextChild = 0;
        StartCoroutine(Scroll_c());
    }

    
}
