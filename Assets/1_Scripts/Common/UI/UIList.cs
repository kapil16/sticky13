﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

//[AddComponentMenu("UI/List")]
//[RequireComponent(typeof(ToggleGroup), typeof(ScrollRect))]
using UnityEngine.Events;


public class UIList : MonoBehaviour {

    /*
    How to create one successfullly
    Create GameObject, assign this to that
    Create a child, add scrollrect, image and mask component to that. This is template.
    Create a child of prev child, assign layoutGroup to this, also content size fitter. This is content of above scrollrect
    Create a child of prev child, assign layout element to this. This is you item.
    
    Template will be copied on initialization.
    Item will be copied to the copied template every time new item is created. 
    */

    public Transform template;
    public Transform item; 
    [HideInInspector]
    public List<GameObject> items = new List<GameObject>();
    [HideInInspector]
    public Transform refTemplate;
    Transform itemsParent; 
    public GameObject noItemsIndicator; 
    public UnityEvent onItemsUpdated;

    public bool supportsSnapping; 
    [HideInInspector]
	public int snappedToItem = 0;
    Interpolate.Position anim = null;


    void OnEnable()
    {
        Initialize();
        SnapToItem(0, false);
        InitializeCulling(); 
    }

    void Initialize() {
        if (refTemplate == null)
        {
            refTemplate = template.Duplicate();
            refTemplate.gameObject.name = "RefTemplate";
            refTemplate.gameObject.SetActive(false); 
            refTemplate.GetChild(0).ClearChildren();
            item.SetParent(refTemplate.GetChild(0));
            itemsParent = template.GetChild(0);
            template.gameObject.SetActive(true);
            ClearItems();
        } 
    }

    public void ClearItems()
    {
        Initialize();
        SnapToItem(0,false);
        itemsParent.ClearChildren();
        items.Clear();
        UpdateNoItemsIndicator();
        onItemsUpdated.Invoke();
    }

    public void AddNewItems(int c) 
	{
        for (int i = 0; i < c; i++)
        {
            AddNewItem();
        }
    }

    public GameObject AddNewItem() 
    {
        Initialize();  
        Transform newItem = item.Duplicate(itemsParent);
        items.Add(newItem.gameObject);
        //SnapToItem (0);
        UpdateNoItemsIndicator();
        onItemsUpdated.Invoke();
        return newItem.gameObject; 
    }

	public void RemoveItem(int i)
	{
		Destroy (items [i]);
		items.RemoveAt (i);
        UpdateNoItemsIndicator();
        onItemsUpdated.Invoke();
	} 

    void UpdateNoItemsIndicator()
    {
        if(noItemsIndicator != null) noItemsIndicator.SetActive(items.Count == 0);
    }

    public void SnapToNextItem() {
        if (snappedToItem==itemsParent.childCount-1)
        {
            return;
        }
        SnapToItem(++snappedToItem);
    }

    public void SnapToPrevtItem()
    {
        if (snappedToItem == 0)
        {
            return;
        }
        SnapToItem(--snappedToItem);
    }

    public void SnapToItem(int i, bool animate = true) {
        if (!supportsSnapping) return;

        if (itemsParent.childCount==0)
        {
            itemsParent.localPosition = Vector3.zero;
            return;
        }
        if (i >= itemsParent.childCount)
        {
            i = itemsParent.childCount - 1;
        }
        if (i < 0)
        {
            i = 0;
        }
        snappedToItem = i;
        if (anim!=null) anim.Stop();
		if (animate) anim = new Interpolate.Position(itemsParent, itemsParent.localPosition, -itemsParent.GetChild(snappedToItem).localPosition, .3f, true, CurvesCollection.linear);
        else itemsParent.localPosition = -itemsParent.GetChild(snappedToItem).localPosition;
    } 

 
#region Culling 
    ScrollRect scrollRect; 
    public float cullRange; 

    void InitializeCulling()
    {
        if (cullRange == 0) return;  
        if (scrollRect==null)
        {
            scrollRect = template.GetComponent<ScrollRect>();
            if (scrollRect != null)
            {  
                itemsParent = scrollRect.content.transform;
                scrollRect.onValueChanged.AddListener(ScrollCull);  
            } 
        }
    }

//    int o = 0;
    public void ScrollCull(Vector2 v2)
    { 
//        o = 0;
        for (int i = 0; i < itemsParent.childCount; i++)
        {
            itemsParent.GetChild(i).GetChild(0).gameObject.SetActive(Mathf.Abs(scrollRect.transform.position.x - itemsParent.GetChild(i).position.x) < cullRange);
//            o++;
        }
//        print(o);
    }
#endregion
}
