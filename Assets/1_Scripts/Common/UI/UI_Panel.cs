﻿using UnityEngine;
using System.Collections;

public class UI_Panel : MonoBehaviour {
	
	void OnEnable(){ 
		WhenScriptsGetEnable ();
	}   

	public void WhenScriptsGetEnable(){ 
		for(int A = 0 ; A < gameObject.transform.childCount ; A++){
			if (A != 0 && A != 1) {
					transform.GetChild (A).gameObject.SetActive (false);
					//Debug.Log ("first two game object set activate ::");
			} else {
					//Debug.Log ("Else game object set Deactivated ::");
					transform.GetChild (0).gameObject.SetActive (true);
					transform.GetChild (1).gameObject.SetActive (true);
			}
		} 
	}

    // Activating Item On Click Using Simple script
    Interpolate.Scale animOpen = null, animClose=null;
    bool animating=false;
	public void ActivateMenuItem(int GetNumber)
    {
        //if (animating) return; 
        //animating = true;

        if (animOpen != null) animOpen.Stop();
        //if (animClose != null) animClose.Stop();
        if (GetNumber < transform.childCount)
        {
            transform.GetChild(GetNumber).gameObject.SetActive(true);
            //animOpen = new Interpolate.Scale(transform.GetChild(GetNumber), Vector3.one*.95f, Vector3.one, .2f);
        }
        else Debug.LogError("Transform Child Out Of Bounds");

		for(int X = 1 ; X < gameObject.transform.childCount ; X++){
            if (X != GetNumber)
            {
                transform.GetChild(X).gameObject.SetActive(false);
                //if (transform.GetChild(X).gameObject.activeSelf)
                //{
                //    animClose = new Interpolate.Scale(transform.GetChild(GetNumber), Vector3.one, Vector3.one * .95f, .2f);
                //    Delayed.Function(() =>
                //    {
                //        transform.GetChild(X).gameObject.SetActive(false);
                //        animating = false;
                //    }, .2f);
                //}
                //         else {
                //		Debug.Log ("Child Cannot Be Activated due to reasons : " +X);  // child cannot be activated becoz can be getnumber or will be 0 child
                //}
            }
		}



	}


		public void WhenBackButtonCall(bool GetTrue){		// On Main Back Button Click

				if(GetTrue == true){
						for(int Y = 0 ; Y <transform.childCount ; Y++ ){
								if (Y == 0) {
										transform.GetChild (0).gameObject.SetActive (true);
										Debug.Log (" Child Name Condition 1 :  " +Y);
								} else if (Y == 1) {
										transform.GetChild (1).gameObject.SetActive (true);
										Debug.Log (" Child Name Condition 2 : " +Y);
								} else {								
										transform.GetChild (Y).gameObject.SetActive (false);
										Debug.Log (" Child Name Condition 3 : " +Y);
								}
						}
						gameObject.SetActive (false);
						Debug.Log ("Disabling Main Object");
				}	
		
		}
				

	
}
