﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LayoutElementTween : MonoBehaviour 
{ 
    public LayoutElement layoutElement;
    public bool beginOnEnable; 
    public float duration = 1;  
    public float widthFrom = -1, widthTo = -1, heightFrom = -1, heightTo = -1;  
    public AnimationCurve curve = AnimationCurve.Linear(0, 0, 1, 1);
    float wf, wt = -1, hf, ht = -1, d = 0;  

    void OnEnable()
    {
        if (layoutElement == null) layoutElement = GetComponent<LayoutElement>();
        if (beginOnEnable) StartTween();
    }

    public void StartTween(bool reverse = false, float tweenDuration = -1)
    {  
        wf = reverse ? widthTo : widthFrom;
        wt = reverse ? widthFrom : widthTo;
        ht = reverse ? heightFrom : heightTo;
        hf = reverse ? heightTo : heightFrom;
        d = (tweenDuration == -1) ? duration : tweenDuration;
        if ((wt == -1 || layoutElement.preferredWidth == wt) && (ht == -1 || layoutElement.preferredHeight == ht)) return; 
        if (d == 0 || !gameObject.activeSelf)
        {
            if (wt > -1) layoutElement.preferredWidth = wt;
            if (ht> -1) layoutElement.preferredHeight = ht; 
        }
        else
        {
            StopCoroutine("Tween");
            StartCoroutine("Tween");
        }
    }

    float timeElapsed, lf;
    IEnumerator Tween() { 
        timeElapsed = 0;  
        while (timeElapsed < d)
        {
            lf = curve.Evaluate(timeElapsed / d);
            if (wt > -1)
                layoutElement.preferredWidth = Mathf.Lerp(wf, wt, lf);
            if (ht > -1)
                layoutElement.preferredHeight = Mathf.Lerp(hf, ht, lf); 
            yield return null;
            timeElapsed += Time.deltaTime; 
        }  
        if (wt > -1) layoutElement.preferredWidth = wt;
        if (ht> -1) layoutElement.preferredHeight = ht; 
    }
}

