﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class UIClickSound : MonoBehaviour, IPointerClickHandler
{
    public string soundKey = "BtnClick";
    public void OnPointerClick(PointerEventData eventData)
    { 
        AudioPlayer.PlaySFX(soundKey);
    } 
}
