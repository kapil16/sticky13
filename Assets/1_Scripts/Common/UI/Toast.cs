﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Toast : MonoBehaviour
{
    public Text msgText;

    static Toast _instance;
    static Toast instance
    {
        get
        {
            if (_instance == null)
            {
                FindObjectOfType<Toast>().GetComponent<Toast>().init();
            }
            return _instance;
        }
        set
        {
            _instance = value;
        }
    }

    static Transform elements;
    static List<string> msgs = new List<string>();
    static Interpolate.Position anim;
    float displayTime = 1;

    void OnEnable()
    {
        init();
    }

    public void init()
    {
        instance = this;
        elements = transform.GetChild(0);
        //Dismiss();
    }

    //int a = 0;
    //void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.T))
    //    {
    //        Show("A Toast! "+a.ToString());
    //        a++;
    //    }
    //    //print(anim != null && anim.isRunning);
    //}

    public static void Show(string msg)
    {
        msgs.Add(msg);
        if (msgs.Count==1) instance.ShowIns(msgs[0]);
    }

    void ShowIns(string msg)
    {
        elements.gameObject.SetActive(true); 

        msgText.text = msg;

        if (anim != null) anim.Stop();
        anim = new Interpolate.Position(elements, new Vector3(0, -50, 0), Vector3.zero, .6f, true);
        Delayed.Function(()=> {
            elements.gameObject.SetActive(false);
            msgs.RemoveAt(0);
            if (msgs.Count > 0) instance.ShowIns(msgs[0]);
            }, displayTime+.3f);
    }

    //public static void Dismiss()
    //{
    //    elements.gameObject.SetActive(false);
    //}

    //public void BtnPressed(int indx)
    //{
    //    if (current.btnsAction[indx] != null) current.btnsAction[indx]();
    //}
}
