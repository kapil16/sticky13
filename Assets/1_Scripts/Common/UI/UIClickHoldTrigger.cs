﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class UIClickHoldTrigger : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public UnityEvent onClick; 
    public UnityEvent onHold;
    public float holdThreshold = .75f;

    bool actionMade = false;
    Delayed.Action holdThresholdCallback;
    float pointerDownTime = 0;

    public void OnPointerDown(PointerEventData eventData)
    {
        pointerDownTime = Time.realtimeSinceStartup;
        actionMade = false;
        if(holdThresholdCallback != null) holdThresholdCallback.CancelAction();
        holdThresholdCallback = new Delayed.Action(OnHoldThreshold, holdThreshold);
    }

    void OnHoldThreshold()
    {
        if (actionMade) return;
        actionMade = true;
        onHold.Invoke();
    }

    public void OnPointerUp(PointerEventData eventData)
    { 
        if (actionMade) return;

        if(holdThresholdCallback != null) holdThresholdCallback.CancelAction();

        actionMade = true;
        if (Time.realtimeSinceStartup - pointerDownTime < holdThreshold)
        { 
            onClick.Invoke();
        }
    }
}
