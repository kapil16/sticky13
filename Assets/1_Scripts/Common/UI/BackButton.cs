﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BackButton : MonoBehaviour {
    Button _btn;
    Button btn {
        get {
            if (_btn == null) _btn = GetComponent<Button>();
            return _btn;
        }
    }

    void OnEnable() {
        BackButtonHandler.Register(btn); 
    }

    void OnDisable()
    {
        BackButtonHandler.Remove(btn);
    }
}
