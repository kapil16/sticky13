﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ClockEvent
{
    public DateTime invokeTime;
    public Action action; 

    public ClockEvent(DateTime _invokeTime, Action _action)
    {
        action = _action;
        invokeTime = _invokeTime;
    }
}

public class GameClock : MonoBehaviour 
{ 
    static bool instantiated;
    static List<ClockEvent> allEvents = new List<ClockEvent>(); 
    static List<int> hourlyEventIndexes = new List<int>(); 
    static int lastUpdatedHour = -1; 

    void OnEnable()
    {
        DontDestroyOnLoad(gameObject);
        StartCoroutine("Clock_c");
        gameObject.name = "GameClock";
        instantiated = true; 
    } 

    void OnApplicationPause(bool paused)
    { 
        if (!paused)
        {
            StopCoroutine("Clock_c");  
            StartCoroutine("Clock_c"); 
        } 
    } 

    void OnApplicationFocus (bool focused)
    {  
        if (focused)
        {
            StopCoroutine("Clock_c");  
            StartCoroutine("Clock_c"); 
        } 
    }


    public static void AddEvent(ClockEvent evt)
    {
        if (!instantiated)
        {
            new GameObject().AddComponent<GameClock>();
        }
        allEvents.Add(evt);
        UpdateHourlyList();
//        print("Event Added" + evt.invokeTime);
    }

    public static void RemoveEvent(int i)
    {
        if (!instantiated)
        {
            new GameObject().AddComponent<GameClock>();
        }
        if (i < allEvents.Count)
        {
            allEvents.RemoveAt(i);
//            print("Event Removed");
        }
        UpdateHourlyList();
    }

    public static void UpdateHourlyList()
    {
        hourlyEventIndexes.Clear();
        for (int i = 0; i < allEvents.Count; i++)
        { 
            double m = allEvents[i].invokeTime.Subtract(DateTime.Now).TotalMinutes;
            if (m >= 0 && m < 65)
            {
                hourlyEventIndexes.Add(i);
            }
        }
    }

    public static DateTime GetFutureTime(int seconds)
    {
        return DateTime.Now.Add(TimeSpan.FromSeconds(seconds));
    }

    IEnumerator Clock_c()
    {
//        yield return new WaitForSeconds(60);
        while (true)
        { 
//            Debug.LogFormat("Clock events pending: {0}. Checked On: {1}", hourlyEvents.Count, DateTime.Now.ToLongTimeString());

            int h = System.DateTime.Now.Hour; 

            if (System.DateTime.Now.Hour != lastUpdatedHour)
            {
                lastUpdatedHour = h;
                UpdateHourlyList();
            }
 
            for (int i = 0; i < hourlyEventIndexes.Count; i++)
            {
                if (allEvents[hourlyEventIndexes[i]].invokeTime.Minute == System.DateTime.Now.Minute)
                {
                    if (allEvents[hourlyEventIndexes[i]].invokeTime.Second == System.DateTime.Now.Second)
                    {
                        try
                        {
                            allEvents[hourlyEventIndexes[i]].action();
                        }
                        catch { }
                        RemoveEvent(hourlyEventIndexes[i]);
                    } 
                }
            }

//            yield return new WaitForSeconds(60 - System.DateTime.Now.Second);
            yield return new WaitForSeconds((1000-System.DateTime.Now.Millisecond)/1000); 
        }
    } 
}

