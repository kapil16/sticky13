﻿using UnityEngine;
using System.Collections;
using System.IO;

public class Screenshot : MonoBehaviour {
    public static string fileNamePrefix = "TeamPokers_";

    public static string captureLocation
    {
        get
        {
            return Application.persistentDataPath;
        }
    }

    public static string saveLocation {
        get
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            string str = "";
            string[] pdp = Application.persistentDataPath.Split('/');
            int s = 0;
            for (int i = 0; i < pdp.Length; i++)
            {
                if (pdp[i].Contains("Android") || pdp[i].Contains("android"))
                {
                    s = i-1;
                    break;
                }
            }
            for (int i = 0; i <= s; i++)
            {
                str += pdp[i] + "/";
            }
            str += "TeamPokers";
            return str; 
#else
            return Application.dataPath;
#endif
        }
    } 

    public class Process{
        string fileName;

        public Process() {
            fileName = newFileName();
            string s = fileName.Substring(1);
            ScreenCapture.CaptureScreenshot(s);
            Debug.Log("Taking SS: "+ s);
            CheckForFile();
        }

        void CheckForFile() {
            if (File.Exists(captureLocation + fileName))
            {
                Debug.Log("Moving File: " + Application.persistentDataPath + fileName + " - " + saveLocation + fileName);
                try
                {
                    Debug.Log(Application.persistentDataPath + fileName + " - " + saveLocation + fileName);
                    if (!Directory.Exists(saveLocation))
                    {
                        Debug.Log("Directory '"+saveLocation+ "' not found, attempting to create.");
                        Directory.CreateDirectory(saveLocation);
                    }
                    File.Move(captureLocation + fileName, saveLocation + fileName);
#if UNITY_ANDROID && !UNITY_EDITOR
                    RefreshAndroidGallery(saveLocation + fileName);
#endif
                }
                catch (System.Exception e)
                {
                    Debug.Log(e.Message);
                }
            }
            else
            {
                Delayed.Function(CheckForFile, 1);
                Debug.Log("File not there("+ captureLocation + fileName + ") yet");
            }
        }
    }

    void OnEnable() {
        Delayed.Function(Take, 1);
    }

    IEnumerator aaaa() {
        yield return new WaitForSeconds(1);
        Take();
    }

    public static void Take() {
        new Process();
    }

    public static string newFileName()
    {
        return
            "/"
            + fileNamePrefix
            + System.DateTime.Now.Year.ToString("0000")
            + System.DateTime.Now.Month.ToString("00")
            + System.DateTime.Now.Day.ToString("00")
            + System.DateTime.Now.Hour.ToString("00")
            + System.DateTime.Now.Minute.ToString("00")
            + System.DateTime.Now.Second.ToString("00")
            + System.DateTime.Now.Millisecond.ToString("00")
            + ".png";
    }

    static void RefreshAndroidGallery(string fileLoc)
    {
        //int androidVersion = 0;
        //AndroidJavaClass classPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        //AndroidJavaClass classVersion = classPlayer.GetStatic<AndroidJavaClass>("android.os.Build.VERSION");
        ////AndroidJavaClass classVersion = classBuild.GetStatic<AndroidJavaClass>();
        //androidVersion = classVersion.GetStatic<int>("SDK_INT");
        //Logs.Add.Info("AndroidVersion: " + androidVersion);


        AndroidJavaClass classPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject objActivity = classPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaClass classUri = new AndroidJavaClass("android.net.Uri");
        AndroidJavaObject objIntent = new AndroidJavaObject("android.content.Intent", new object[2] { "android.intent.action.ACTION_MEDIA_MOUNTED", classUri.CallStatic<AndroidJavaObject>("parse", "file://" + fileLoc) });

        AndroidJavaObject objContenUri = classUri.CallStatic<AndroidJavaObject>("fromFile", "file://" + fileLoc);
        objIntent = new AndroidJavaObject("android.content.Intent", new object[2] { "android.intent.action.ACTION_MEDIA_SCANNER_SCAN_FILE", classUri.CallStatic<AndroidJavaObject>("parse", "file://" + fileLoc) });
        objIntent.CallStatic("setData", objContenUri);
        try
        {
            objActivity.Call("sendBroadcast", objIntent);
            Debug.Log("Refreshed G with MSSF");
        }
        catch
        {
            Debug.Log("Failed SS with MSSF, failed to refresh.");
        }
        //try
        //{
        //    objActivity.Call("sendBroadcast", objIntent);
        //    Logs.Add.Info("Refreshed G with MM");
        //}
        //catch
        //{
        //    Logs.Add.Error("Failed SS with MM, attempting with MSSF");
        //    AndroidJavaClass contenUri = new AndroidJavaClass("android.net.Uri").CallStatic<AndroidJavaClass>("fromFile", "file://" + fileLoc);
        //    objIntent = new AndroidJavaObject("android.content.Intent", new object[2] { "android.intent.action.ACTION_MEDIA_SCANNER_SCAN_FILE", classUri.CallStatic<AndroidJavaObject>("parse", "file://" + fileLoc) });
        //    objIntent.CallStatic("setData", contenUri);
        //    try
        //    {
        //        objActivity.Call("sendBroadcast", objIntent);
        //        Logs.Add.Info("Refreshed G with MSSF");
        //    }
        //    catch
        //    {
        //        Logs.Add.Error("Failed SS with MSSF, failed to refresh.");
        //    }
        //}
    }
     

    //private const string MediaStoreImagesMediaClass = "android.provider.MediaStore$Images$Media";
    //public static string SaveImageToGallery(Texture2D texture2D, string title, string description)
    //{
    //    using (var mediaClass = new AndroidJavaClass(MediaStoreImagesMediaClass))
    //    {
    //        using (var cr = Activity.Call<AndroidJavaObject>("getContentResolver"))
    //        {
    //            var image = Texture2DToAndroidBitmap(texture2D);
    //            var imageUrl = mediaClass.CallStatic<string>("insertImage", cr, image, title, description);
    //            return imageUrl;
    //        }
    //    }
    //}

    //public static AndroidJavaObject Texture2DToAndroidBitmap(Texture2D texture2D)
    //{
    //    byte[] encoded = texture2D.EncodeToPNG();
    //    using (var bf = new AndroidJavaClass("android.graphics.BitmapFactory"))
    //    {
    //        return bf.CallStatic<AndroidJavaObject>("decodeByteArray", encoded, 0, encoded.Length);
    //    }
    //}
}
