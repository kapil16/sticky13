﻿using UnityEngine;
using System.Collections;

public class TexturesCollection : MonoBehaviour 
{
    static Texture2D _whiteTexture;
    public static Texture2D whiteTexture
    {
        get{ 
            if (_whiteTexture == null)
            {
                _whiteTexture = new Texture2D(1, 1);
                _whiteTexture.SetPixel(0, 0, Color.white);
                _whiteTexture.Apply();
            }
            return _whiteTexture;
        }
    }

    static Texture2D _blackTexture;
    public static Texture2D blackTexture
    {
        get{ 
            if (_blackTexture == null)
            {
                _blackTexture = new Texture2D(1, 1);
                _blackTexture.SetPixel(0, 0, Color.black);
                _blackTexture.Apply();
            }
            return _blackTexture;
        }
    }

    static Texture2D _transparentTexture;
    public static Texture2D transparentTexture
    {
        get{ 
            if (_transparentTexture == null)
            {
                _transparentTexture = new Texture2D(1, 1);
                _transparentTexture.SetPixel(0, 0, Color.black.transparent(1));
                _transparentTexture.Apply();
            }
            return _transparentTexture;
        }
    }


    static Sprite _transparentSprite;
    public static Sprite transparentSprite
    {
        get{ 
            if (_transparentSprite==null)
            {
                _transparentSprite = Sprite.Create(transparentTexture, new Rect(0, 0, 1, 1), new Vector2(0.5f, 0.5f)); 
            }
            return _transparentSprite;
        }
    }
}
