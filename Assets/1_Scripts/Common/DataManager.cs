﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class DataManager : MonoBehaviour
{
    static DataManager instance;
    void saveIns() {
        save();
    }
    public static PersistedData persistentData = new PersistedData();
    public PersistedData data = new PersistedData();
    static string fileLocation;
    void Awake () {
        instance = this;
        persistentData = data;
        fileLocation = Application.persistentDataPath + "/screenshot.png";
        load();
        //print(fileLocation);
    }

    public static void save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(fileLocation,FileMode.OpenOrCreate);

        bf.Serialize(file, persistentData);
        file.Close();
    }

    public static void load() {
        if (File.Exists(fileLocation))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(fileLocation, FileMode.Open);
            persistentData = (PersistedData)bf.Deserialize(file);
            instance.data = persistentData;
            file.Close();
        }
    }
}

[System.Serializable]
public class PersistedData
{
    public string gameVersion; 
    public int bestScore;
    public string u, p;
}
