﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SheetAnimation : MonoBehaviour 
{ 
    public Texture2D animSheet;
    public Image image;
    public int xCount = 1, yCount = 1;
    public int fps = 15;
    public bool playOnEnable;
    public bool loop;

    Sprite[] sprites = new Sprite[0];
    int currentSprite = 0; 

    void OnEnable()
    {
        if(playOnEnable) Play();
    }

    public void SetImage(string _sheetUrl, int _xCount, int _yCount)
    {
        if (string.IsNullOrEmpty(_sheetUrl)) return;

        xCount = _xCount;
        yCount = _yCount;
        sprites = new Sprite[0]; 

        new Download.Image(
            _sheetUrl, 
            true,
            (tex)=>
            { 
                animSheet = tex; 
                CreateSprites();
            },
            null
        );  
    }

    public void SetImage(Texture2D _sheet, int _xCount, int _yCount, bool play = false)
    {  
        animSheet = _sheet;
        xCount = _xCount;
        yCount = _yCount;
        sprites = new Sprite[0];
        CreateSprites();

        if (play) Play();
    }

    public void Play()
    {
        if (image == null) return;
        if (sprites.Length == 0) CreateSprites(); 
        if (sprites.Length == 0) return;

        currentSprite = 0;
        StopCoroutine("Anim_c");
        StartCoroutine("Anim_c");
    }

    IEnumerator Anim_c()
    { 
        float wait = 1f / fps;
        while (true)
        {
            if (currentSprite >= sprites.Length)
            {
                if (loop)
                    currentSprite = 0;
                else
                    yield break;
            }
            image.sprite = sprites[currentSprite++]; 
            yield return new WaitForSeconds(wait);  
        }
    }

    void CreateSprites()
    {
        if (animSheet == null) return;

        float w = animSheet.width/xCount;
        float h = animSheet.height/yCount;
        sprites = new Sprite[xCount * yCount];
        int ii = 0;
        for (int j = yCount-1; j >= 0; j--) 
        {
            for (int i = 0; i < xCount; i++)
            { 
                sprites[ii++] = Sprite.Create(animSheet, new Rect(i * w, j * h, w, h), new Vector2(0.5f, 0.5f)); 
            }
        }
    }
}
