﻿using UnityEngine;
using System.Collections;

public class CurveTweenRandom : MonoBehaviour {
	public CurveTween tweenComp;

	public Vector3 val;
	public float multiplierMin, multiplierMax = 1; 

	public float duration=1;
	public float durationMultiplierMin, durationMultiplierMax = 1;

	void OnEnable(){
		if(tweenComp!=null)StartCoroutine ("Loop");	
	}

	IEnumerator Loop(){
		float d = duration * Random.Range (durationMultiplierMin, durationMultiplierMax); 
		tweenComp.startPoint.valAbsolute = tweenComp.endPoint.valAbsolute;
		tweenComp.endPoint.valAbsolute = val * Random.Range (multiplierMin, multiplierMax);
		tweenComp.duration = d;
		tweenComp.StartTween ();
		yield return new WaitForSeconds (d);  
		StartCoroutine ("Loop");	
	}
}
