﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ImageEffectFill : MonoBehaviour 
{
	public Image imgComp;
	public float defDuration = 1;
	float currDuration;  
	public float targetFill = 1; 
	float startFill = 0;
	float currentFill = 0;
	public bool beginOnEnable;

	void OnEnable(){
		//Begin (0,.5f,0,Color.red);
		if (beginOnEnable) {
			Begin ();
		}
	}

	void Set(float fillFrom = 0, float fillTo = 1, float duration = 0, Color color = default(Color))
	{
		currDuration = duration == 0 ? defDuration : duration; 
		startFill = currentFill = fillFrom;
		targetFill = fillTo;
		if (color !=default(Color)) imgComp.color = color; 
	}

	void Begin()
	{ 
		if(currDuration==0) currDuration = defDuration;
		StopCoroutine ("Effect");
		StartCoroutine ("Effect");
	}

	void Begin(float fillFrom, float fillTo, float duration = 0, Color color = default(Color))
	{
		Set(fillFrom, fillTo, duration, color);

		StopCoroutine ("Effect");
		StartCoroutine ("Effect");
	}

	IEnumerator Effect()
	{
		float t = 0;
		while (t < currDuration) 
		{
			currentFill = Mathf.Lerp (startFill, targetFill, t / currDuration);	
			imgComp.fillAmount = currentFill;
			t += Time.deltaTime;
			yield return null;
		}	
		currentFill = targetFill;	
		imgComp.fillAmount = currentFill;
	}
}
