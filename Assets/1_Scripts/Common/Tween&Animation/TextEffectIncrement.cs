﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextEffectIncrement : MonoBehaviour { 
	public Text textComp;
	public float defDuration = 1;
	float currDuration; 
	double startVal;
	double currentVal;
	double targetVal;
	public string prefix;

	public bool beginOnEnable;

	void OnEnable(){
//		Begin (0, 1000000, 0);
		if (beginOnEnable) {
			Begin ();
		}
	}

	void Begin(double fromVal, double toVal, float duration = 0)
	{
		Set(fromVal, toVal, duration);
		Begin ();
	}

	public void Set(double fromVal, double toVal, float duration = 0)
	{
		currDuration = duration == 0 ? defDuration : duration;
		startVal = currentVal = fromVal;
		targetVal = toVal;
	} 

	void Begin()
	{ 
		StopCoroutine ("Effect");
		StartCoroutine ("Effect");
	}

    public AudioSource audio; 
    public AudioClip loopSound,endSound;
	IEnumerator Effect()
	{
//		print (startVal + " to " + targetVal);
		float t = 0;
        if (audio!=null)
        {
            audio.playOnAwake = false;
            audio.loop = true;
            if (loopSound != null)
            {
                audio.clip = loopSound;
                audio.Stop(); 
                audio.Play();
            }
        }
		while (t < currDuration) 
		{
			currentVal = NumbersExt.Lerp (startVal, targetVal, t / currDuration);	
			textComp.text = prefix + currentVal.ToString ("0");
			t += Time.deltaTime;
			yield return null;
		}	
		currentVal = targetVal;	
        textComp.text = prefix + currentVal.ToString ();
        if (audio!=null)
        { 
            audio.loop = false;
            audio.Stop();
            if(endSound!=null) audio.PlayOneShot(endSound);
        }
	}
}
