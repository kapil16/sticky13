﻿using UnityEngine;
using System.Collections;

public class CanvasGroupFade : MonoBehaviour 
{ 
    public enum OnEnableBehaviour { DoNothing, FadeIn, FadeOut }
    public CanvasGroup canvasGroup;
    public float duration = 1;
    public AnimationCurve curve = AnimationCurve.Linear(0,0,1,1); 
    public OnEnableBehaviour onEnable;

    public void OnEnable()
    {
        if (canvasGroup == null) canvasGroup = GetComponent<CanvasGroup>();
        if (canvasGroup != null)
        {
            switch (onEnable)
            { 
                case OnEnableBehaviour.FadeIn:
                    FadeIn();
                    break;

                case OnEnableBehaviour.FadeOut:
                    FadeOut();
                    break;

                default:
                    break;
            }
        }
    }

    public void FadeIn(float _duration = -1)
    {  
        if (!gameObject.activeSelf || canvasGroup == null) return;
        v1 = 0;
        v2 = 1;
        d = _duration == -1 ? duration : _duration;
        StopCoroutine("Tween");
        StartCoroutine("Tween");
    }

    public void FadeOut(float _duration = -1)
    {
        if (!gameObject.activeSelf || canvasGroup == null) return;
        v1 = 1;
        v2 = 0;
        d = _duration == -1 ? duration : _duration;
        StopCoroutine("Tween");
        StartCoroutine("Tween");
    }

    float v1, v2, d;
    IEnumerator Tween()
    {  
        float timeElapsed = 0; 
        while (timeElapsed<duration)
        {
            timeElapsed += Time.deltaTime; 
            canvasGroup.alpha = Mathf.Lerp(v1, v2, curve.Evaluate(timeElapsed/duration));
            yield return null;
        } 
        canvasGroup.alpha = v2;
    }
}
