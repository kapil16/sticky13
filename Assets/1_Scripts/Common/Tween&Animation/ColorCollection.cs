﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ColorCollection : MonoBehaviour 
{
	public MaskableGraphic graphic;
	public Color[] colors;
	public bool randomizeOnEnable;

	void OnEnable()
	{
		if (randomizeOnEnable) {
			ApplyRandom ();
		}
	}

	void ApplyRandom()
	{
		graphic.color = GetRandom ();	
	}

	public Color GetRandom()
	{
		if (colors.Length == 0) return Color.white;
		return colors [Random.Range (0, colors.Length)];
	}
}
